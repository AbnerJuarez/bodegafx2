package org.abnerjuarez.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
	* Clase tipo beans encargada de administrar los detalles de los prestamos
	* @author Abner A. Juarez D.
*/
public class DetallePrestamo{
	//Propiedades
	private IntegerProperty idPrestamo, idHerramienta, cantidad;
	private StringProperty nombreHerramienta;
	/**
		* Metodo que inicializa las propiedades de la clase
	*/
	public void instanciar(){
		this.idHerramienta = new SimpleIntegerProperty();
		this.idPrestamo = new SimpleIntegerProperty();
		this.cantidad = new SimpleIntegerProperty();
		this.nombreHerramienta = new SimpleStringProperty();
	}
	/**
		* Constructor vacio de la clase
	*/
	public DetallePrestamo(){
		this.instanciar();
	}
	/**
		* Constructor de la clase con todos los parametros
		* @param idPrestamo Int con el id del prestamo al que pertenece el detalle
		* @param idHerramienta Int con el id de la herramienta al que pertenece el detalle
		* @param cantidad Int con la cantidad de herramientas  de un mismo tipo del detalle
		* @param nombreHerramienta Nombre de la herramienta a prestar
	*/
	public DetallePrestamo(int idPrestamo, int idHerramienta, int cantidad, String nombreHerramienta){
		this.instanciar();
		this.setIdPrestamo(idPrestamo);
		this.setIdHerramienta(idHerramienta);
		this.setCantidad(cantidad);
		this.setNombreHerramienta(nombreHerramienta);
	}
	/**
		* Metodo para settear el idPrestamo del detalle
		* @param idPrestamo Int con el idPrestamo del detalle
	*/
	public void setIdPrestamo(int idPrestamo){
		this.idPrestamo.set(idPrestamo);
	}
	/**
		* Metodo para settear el idHerramienta del detalle
		* @param idHerramienta Int con el idHerramienta del detalle
	*/
	public void setIdHerramienta(int idHerramienta){
		this.idHerramienta.set(idHerramienta);
	}
	/**
		* Metodo par settear la cantidad de herramientas del detalle
		* @param cantidad Int con el valor de la cantidad de herramientas del detalle
	*/
	public void setCantidad(int cantidad){
		this.cantidad.set(cantidad);
	}
	/**
		* Metodo para settear el nombre de la herramienta del detalle
		* @param nombreHerramienta String con el valor del nombre de la herramienta del detalle
	*/
	public void setNombreHerramienta(String nombreHerramienta){
		this.nombreHerramienta.set(nombreHerramienta);
	}
	/**
		* Metodo para obtener el idPrestamo del detalle
		* @return Retorna un int con el valor del IdPrestamo del detalle
	*/
	public int getIdPrestamo(){
		return this.idPrestamo.get();
	}
	/**
		* Metodo para obtener el idHerramienta del detalle
		* @return Retorna un int con el valor del idHerramienta del detalle
	*/
	public int getIdHerramienta(){
		return this.idHerramienta.get();
	}
	/**
		* Metodo para obtener la cantidad de herramientas del mismo tipo del detalle
		* @return Retorna un int con el valor de la cantidad de herramientas del detalle
	*/
	public int getCantidad(){
		return this.cantidad.get();
	}
	/**
		* Metodo para obtener el nombre de la herramienta del detalle
		* @return Retorna un String con el nombre de la herramienta del detalle 
	*/
	public String getNombreHerramienta(){
		return this.nombreHerramienta.get();
	}
}
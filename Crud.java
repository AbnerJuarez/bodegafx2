package org.abnerjuarez.visual;

import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableCell;
import javafx.collections.ObservableList;
import java.util.ArrayList;
import javafx.scene.input.*;
import javafx.event.Event;
import javafx.event.ActionEvent;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.TableCell;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.GridPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.ToolBar;
import javafx.scene.control.CheckBox;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import org.abnerjuarez.db.Conection;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.abnerjuarez.recursos.Buscador;
import org.abnerjuarez.recursos.DragDrop;
import org.abnerjuarez.beans.Empleado;
import org.abnerjuarez.beans.Cliente;
import org.abnerjuarez.beans.Herramienta;
import org.abnerjuarez.beans.Prestamo;
import org.abnerjuarez.recursos.Eliminador;
import org.abnerjuarez.visual.Prestamista;
import org.abnerjuarez.beans.Devolucion;
import org.abnerjuarez.manejadores.ManejadorDevolucion;
import org.abnerjuarez.manejadores.ManejadorPrestamo;
import org.abnerjuarez.manejadores.ManejadorHerramienta;
import org.abnerjuarez.manejadores.ManejadorCliente;
import org.abnerjuarez.manejadores.ManejadorEmpleado;


/**
	* Clase que administra el modulo crud del programa
	* @author Abner A. Juarez D.
*/
public class Crud extends Application implements EventHandler<Event>{
	//Propiedades
	private Scene scene, scene2;
	private Eliminador eliminador;
	private Button btLogin,btagregara, bteliminara, btLogout, desplegar, btAceptarEmpleado, btBuscar;
	private Button izquierda, derecha, prestamista;
	private ToolBar crud;
	private TextField tfUser, tfNombreHerramienta, tfCantidadHerramienta, txtCategoria;
	private TextField tfAddEmpleadoNombre, tfAddEmpleadoApellido, tfAddEmpleadoId;
	private TextField tfcnombre, tfcapellido, tfccarne, tfcseccion;
	private PasswordField pfPassword, pfAddEmpleadoPass;
	private Stage segunda, tercera;
	private Group grup;
	private SplitMenuButton smbHerramientas;
	private BorderPane bp;
	private VBox vb1;
	private CheckBox ckPrestamista, ckCrud, ckVisor, ckMatutinam, ckVespertina;
	private Stage agregar, eliminar, actualizar, addCategoria, stgHerramientas, stgEmpleados, stgClientes;
	private GridPane gpHerramienta, gpEmpleado, gpCliente;
	private HBox hb1, hbHerramientas, hbEmpleados, hbClientes;
	private Conection conect;
	private ManejadorEmpleado mngEmpleado;
	private ManejadorCliente mngCliente;
	private ManejadorHerramienta mngHerramienta;
	private ManejadorPrestamo mngPrestamo;
	private ManejadorDevolucion mngDevolucion;
	private TabPane tbPrincipal;
	private Tab tbEmpleados, tbClientes, tbHerramientas, tbPrestamos, tbDevoluciones;
	private TableView<Empleado> tvEmpleados;
	private TableView<Cliente> tvClientes;
	private TableView<Herramienta> tvHerramientas;
	private TableView<Prestamo> tvPrestamos;
	private TableView<Devolucion> tvDevoluciones;
	private ResultSet result;
	private boolean veces;
	private Buscador buscador;
	private DragDrop dragDrop;
	/**
		* Metodo que carga la ventana principal heredado de Application
		* @param primary Stage de la ventana primaria
	*/
	public void start(Stage primary){
		this.iniciar();
	}
	/**
		* Constructor con todos los manejadores de la clase Crud
		* @param me ManejadorEmpleado a inyectar en la clase
		* @param mc ManejadorCliente a inyectar en la clase
		* @param mp ManejadorPrestamo a inyectar en la clase
		* @param mh ManejadorHerramienta a inyectar en la clase
	*/
	public Crud(ManejadorEmpleado me, ManejadorCliente mc, ManejadorPrestamo mp, ManejadorHerramienta mh){
		this.mngEmpleado = me; 
		this.mngCliente = mc;
		this.mngHerramienta = mh; 
		this.mngPrestamo = mp;
		this.dragDrop = new DragDrop();
	}
	/**
		* Metodo que se inicia al lanzar la aplicacion, instancia la mayoria de propiedades
	*/
	public void iniciar(){
		this.eliminador = new Eliminador(mngHerramienta, mngPrestamo, mngEmpleado, mngCliente);
		this.dragDrop.setEliminador(eliminador);
		tbPrincipal = new TabPane();
		tbPrincipal.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>(){
			@Override
			public void changed(ObservableValue<? extends Tab> arg0, Tab arg1, Tab mostRecentlySelectedTab){
				if(mostRecentlySelectedTab.equals(tbClientes)){
					dragDrop.setTipo("Cliente");
				}
				if(mostRecentlySelectedTab.equals(tbEmpleados)){
					dragDrop.setTipo("Empleado");
				}
				if(mostRecentlySelectedTab.equals(tbHerramientas)){
					dragDrop.setTipo("Herramienta");
				}
			}
		 });
		btagregara = new Button("Agregar");
		bteliminara = new Button("Eliminar");
		btBuscar = new Button("Buscar");
		this.dragDrop.setTarget(bteliminara);
		this.buscador = new Buscador(mngCliente, mngHerramienta, mngEmpleado);
		this.buscador.setCrud(this);
		stgHerramientas = new Stage();
		stgEmpleados = new Stage();
		stgClientes = new Stage();
		btagregara.addEventHandler(ActionEvent.ACTION, this);
		bteliminara.addEventHandler(ActionEvent.ACTION, this);
		btBuscar.addEventHandler(ActionEvent.ACTION, this);
		crud = new ToolBar();
		conect = new Conection();
		desplegar = new Button("+");
		desplegar.setPrefWidth(200);
		this.modulos();
	}
	/**
		* Metodo encargado de manejar los eventos cargados a esta clase
		* @param event Recibe el evento que se produjo en la aplicacion
	*/
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource().equals(desplegar)){
				if(desplegar.getText().equals("+")){
					desplegar.setText("-");
					vb1.getChildren().clear();
					vb1.getChildren().add(crud);
					vb1.getChildren().add(desplegar);
				}else if(desplegar.getText().equals("-")){
					desplegar.setText("+");
					vb1.getChildren().clear();
					vb1.getChildren().add(desplegar);
				}
			}else if(event.getSource().equals(btagregara)){
				if(tbHerramientas.isSelected()==true){
					gpHerramienta = new GridPane();
					tfNombreHerramienta = new TextField();
					tfNombreHerramienta.setPromptText("Marca de la herramienta");
					tfNombreHerramienta.addEventHandler(KeyEvent.KEY_PRESSED, this);
					tfCantidadHerramienta = new TextField();
					tfCantidadHerramienta.addEventHandler(KeyEvent.KEY_PRESSED, this);
					tfCantidadHerramienta.setPromptText("Cantidad de la herramienta");
					smbHerramientas = new SplitMenuButton();
					smbHerramientas.setText("Elija una categoria");
					result = conect.ejecutarConsulta("SELECT categoria FROM Herramienta");
					if(result!=null){
						try{
							smbHerramientas.getItems().add(new MenuItem("Agregar"));
							while(result.next()){
								String texto=result.getString("categoria");
								if(this.existCategoria(texto)==false)
									smbHerramientas.getItems().add(new MenuItem(texto));
							}
							for(MenuItem item:smbHerramientas.getItems()){
								item.setOnAction(new EventHandler<ActionEvent>() {
									@Override public void handle(ActionEvent e) {
								        if(item.getText().equals("Agregar"))
								        	initTxtCategoria();
								        else
						            		smbHerramientas.setText(item.getText());
									}
								});
							}

						}catch(SQLException e){}
					}else{
						this.initTxtCategoria();
					}
					gpHerramienta.add(tfNombreHerramienta, 0 , 0);
					gpHerramienta.add(smbHerramientas, 1, 0);
					gpHerramienta.add(tfCantidadHerramienta, 0, 1);
					Scene scnHerramientas = new Scene(gpHerramienta);
					stgHerramientas.setScene(scnHerramientas);
					stgHerramientas.setTitle("Agregar herramientas");
					stgHerramientas.show();
				}else if(tbEmpleados.isSelected()==true){
					ckVisor = new CheckBox("Visor");
					ckCrud = new CheckBox("CRUD");
					ckPrestamista = new CheckBox("Prestamista");
					hbEmpleados = new HBox();
					hbEmpleados.getChildren().addAll(ckCrud, ckPrestamista, ckVisor);
					gpEmpleado = new GridPane();
					tfAddEmpleadoNombre= new TextField();
					tfAddEmpleadoNombre.addEventHandler(KeyEvent.KEY_PRESSED, this);
					btAceptarEmpleado = new Button("Aceptar");
					btAceptarEmpleado.addEventHandler(ActionEvent.ACTION, this);
					tfAddEmpleadoNombre.setPromptText("Nombre");
					tfAddEmpleadoApellido = new TextField();
					tfAddEmpleadoApellido.addEventHandler(KeyEvent.KEY_PRESSED, this);
					tfAddEmpleadoApellido.setPromptText("Apellido");
					tfAddEmpleadoId = new TextField();
					tfAddEmpleadoId.addEventHandler(KeyEvent.KEY_PRESSED, this);
					tfAddEmpleadoId.setPromptText("Identificador");
					pfAddEmpleadoPass = new PasswordField();
					pfAddEmpleadoPass.addEventHandler(KeyEvent.KEY_PRESSED, this);
					pfAddEmpleadoPass.setPromptText("Password");
					gpEmpleado.add(tfAddEmpleadoNombre, 0, 0);
					gpEmpleado.add(tfAddEmpleadoApellido, 0, 1);
					gpEmpleado.add(tfAddEmpleadoId, 1, 0);
					gpEmpleado.add(pfAddEmpleadoPass, 1, 1);
					gpEmpleado.add(hbEmpleados, 0, 2, 2, 1);
					gpEmpleado.add(btAceptarEmpleado, 1, 3);
					Scene scnEmpleados = new Scene(gpEmpleado);
					stgEmpleados.setScene(scnEmpleados);
					stgEmpleados.setTitle("Agregar empleados");
					stgEmpleados.show();
				} else if(tbClientes.isSelected()==true){
					this.agregarClienteCrud();
				}
			} else if(event.getSource().equals(bteliminara)){
				if(tbHerramientas.isSelected()==true){
					ObservableList<Herramienta> herramientaSeleccionados = tvHerramientas.getSelectionModel().getSelectedItems();
					ArrayList<Herramienta> listHerramienta = new ArrayList<Herramienta>(herramientaSeleccionados);			
					for(Herramienta herramienta : listHerramienta){
						eliminador.eliminarHerramienta(herramienta.getIdHerramienta());
					}
				} else if(tbEmpleados.isSelected()==true){ 
					ObservableList<Empleado> empleadoSeleccionados = tvEmpleados.getSelectionModel().getSelectedItems();
					ArrayList<Empleado> listEmpleado = new ArrayList<Empleado>(empleadoSeleccionados);			
					for(Empleado empleado : listEmpleado){
						eliminador.eliminarEmpleado(empleado.getIdUsuario());
					}
				}else if(tbClientes.isSelected()==true){
					ObservableList<Cliente> clienteSeleccionados = tvClientes.getSelectionModel().getSelectedItems();
					ArrayList<Cliente> listCliente = new ArrayList<Cliente>(clienteSeleccionados);			
					for(Cliente cliente : listCliente){
						eliminador.eliminarCliente(cliente.getIdUsuario());
					}
				}
			}else if(event.getSource().equals(btAceptarEmpleado)){
				if(verificarDatos(tfAddEmpleadoNombre.getText(), tfAddEmpleadoApellido.getText())==false && verificarDatos(tfAddEmpleadoId.getText(), pfAddEmpleadoPass.getText())==false){
					String modulos="";
					if(ckVisor.isSelected()==true)
						modulos=modulos+" visor";
					if(ckPrestamista.isSelected()==true)
						modulos=modulos+" prestamista";
					if(ckCrud.isSelected()==true)
						modulos=modulos+" crud";
					if(!modulos.trim().equals("")){
						mngEmpleado.agregarEmpleado(tfAddEmpleadoNombre.getText(), tfAddEmpleadoApellido.getText(), modulos, pfAddEmpleadoPass.getText(), tfAddEmpleadoId.getText());
						stgEmpleados.close();
					}
				}
			}else if(event.getSource().equals(btBuscar)){
				if(tbClientes.isSelected()==true){
					this.buscador.setTipo("Cliente");
					this.buscador.iniciar();
				}else if(tbEmpleados.isSelected()==true){
					this.buscador.setTipo("Empleado");
					this.buscador.iniciar();
				}else if(tbHerramientas.isSelected()==true){
					this.buscador.setTipo("Herramienta");
					this.buscador.iniciar();
				}
			}
		}else if(event instanceof KeyEvent){
			KeyEvent kevent = (KeyEvent)event;
			if(kevent.getCode() == KeyCode.ENTER){
				if(event.getSource().equals(tfcnombre) || event.getSource().equals(tfcapellido) || event.getSource().equals(tfcseccion) || event.getSource().equals(tfccarne)){
					if(this.obtenerVacio(gpCliente)!=null){
						TextField vacio=this.obtenerVacio(gpCliente);
						vacio.requestFocus();
					}else{
						String jornada;
						if(ckMatutinam.isSelected()==true || ckVespertina.isSelected()==true){
							if(ckMatutinam.isSelected()==true)
								mngCliente.agregarCliente(tfcnombre.getText(), tfcapellido.getText(), tfccarne.getText(), tfcseccion.getText(), "Matutina");
							else if(ckVespertina.isSelected()==true)
								mngCliente.agregarCliente(tfcnombre.getText(), tfcapellido.getText(), tfccarne.getText(), tfcseccion.getText(), "Vespertina");
							stgClientes.close();
						}
					}
				}else if(event.getSource().equals(tfAddEmpleadoNombre) || event.getSource().equals(pfAddEmpleadoPass) || event.getSource().equals(tfAddEmpleadoApellido) || event.getSource().equals(tfAddEmpleadoId)){
					if(this.obtenerVacio(gpEmpleado)!=null){
						TextField vacio=this.obtenerVacio(gpEmpleado);
						vacio.requestFocus();
					}
				}else if(event.getSource().equals(tfNombreHerramienta) || event.getSource().equals(tfCantidadHerramienta)){
					if(this.obtenerVacio(gpHerramienta)!=null){
						TextField vacio=this.obtenerVacio(gpHerramienta);
						vacio.requestFocus();
					}else if(!(smbHerramientas.getText().equals("Elija una categoria"))){
						try{
							int cantidad=Integer.parseInt(tfCantidadHerramienta.getText());
							mngHerramienta.insertarHerramienta(tfNombreHerramienta.getText(), smbHerramientas.getText(), cantidad);
							stgHerramientas.close();
						}catch(NumberFormatException ne){

						}
					}
				}else if(event.getSource().equals(txtCategoria)){
					txtCategoria.requestFocus();
					if(this.existCategoria(txtCategoria.getText())==false){
						smbHerramientas.getItems().add(new MenuItem(txtCategoria.getText()));
						smbHerramientas.setText(txtCategoria.getText());
						this.addCategoria.close();
					}
				}
			}
		}
	}
	/**
		* Metodo que carga una ventana para agregar clientes
	*/
	public void agregarClienteCrud(){
		ckVespertina = new CheckBox("Vespertina");
		ckMatutinam = new CheckBox("Matutina");
		gpCliente = new GridPane();
		tfcapellido = new TextField();
		tfcapellido.addEventHandler(KeyEvent.KEY_PRESSED, this);
		tfcapellido.setPromptText("Apellido");
		tfcnombre = new TextField();
		tfcnombre.addEventHandler(KeyEvent.KEY_PRESSED, this);
		tfcnombre.setPromptText("Nombre");
		tfcseccion = new TextField();
		tfcseccion.addEventHandler(KeyEvent.KEY_PRESSED, this);
		tfcseccion.setPromptText("Seccion");
		tfccarne = new TextField();
		tfccarne.addEventHandler(KeyEvent.KEY_PRESSED, this);
		tfccarne.setPromptText("DPI");
		gpCliente.add(tfcnombre, 0, 1);
		gpCliente.add(tfcapellido, 0, 2);
		gpCliente.add(tfccarne, 1, 1);
		gpCliente.add(tfcseccion, 1, 2);
		hbClientes = new HBox();
		hbClientes.getChildren().addAll(ckMatutinam, ckVespertina);
		gpCliente.add(hbClientes, 0, 0);
		Scene scnClientes = new Scene(gpCliente);
		if(stgClientes==null)
			stgClientes = new Stage();
		stgClientes.setTitle("Agregar Cliente");
		stgClientes.setScene(scnClientes);
		stgClientes.show();
	}
	/**
		* Metodo para obtener los hijos TextField de un GridPane que estan vacios
		* @param gp GridPane del que se verificaran los hijos vacios
		* @return Retorna el hijo TextField que encontro vacio
	*/
	public TextField obtenerVacio(GridPane gp){
		for(Node nodo:gp.getChildren()){
			try{
				TextField txt=(TextField)nodo;
				if(txt.getText().trim().equals("")){
					return txt;
				}
			}catch(Exception e){}
		}
		return null;
	}
	/**
		* Metodo que inicia un Stage para agregar una categoria de herramientas
	*/
	public void initTxtCategoria(){
		addCategoria = new Stage();
		txtCategoria = new TextField();
		txtCategoria.setPromptText("Nueva categoria");
		txtCategoria.addEventHandler(KeyEvent.KEY_PRESSED, this);
		Scene scn = new Scene(txtCategoria);
		addCategoria.setScene(scn);
		addCategoria.setTitle("Agregar nueva categoria");
		addCategoria.showAndWait();
	}
	/**
		* Metodo que verifica si existe una categoria en las herramientas
		* @param categoria String con la categoria a verificar
		* @return Retorna un boolean respectivo a si existe o no
	*/
	public boolean existCategoria(String categoria){
		for(MenuItem item:smbHerramientas.getItems()){
			if(item.getText().equals(categoria))
				return true;
		}
		return false;
	}
	/**
		* Metodo para cargar los botones el control desplegable del modulo crud
	*/
	public void controlDesplegable(){
		vb1 = new VBox();
		bp.setTop(vb1);
		vb1.getChildren().add(desplegar);
		vb1.setAlignment(Pos.TOP_CENTER);
		desplegar.addEventHandler(ActionEvent.ACTION, this);
		crud.getItems().addAll(btagregara, bteliminara, btBuscar);
	}
	/**
		* Metodo encargado de cargar los tableView y los tab del modulo crud
	*/
	public void modulos(){
		if(veces==false){
			bp = new BorderPane();
			this.controlDesplegable();
			tercera = new Stage();
			scene2 = new Scene(bp);
			tvEmpleados = new TableView<Empleado>(mngEmpleado.obtenerEmpleados());
			tvEmpleados.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			tvEmpleados.setEditable(true);

			TableColumn<Empleado, String> columnaNombre=new TableColumn<Empleado, String>("NOMBRE");
			columnaNombre.setCellValueFactory(new PropertyValueFactory<Empleado, String>("nombre"));
			columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn());
			columnaNombre.setMinWidth(90);
			this.activarEditEmpleado(columnaNombre, "nombre", null);

			TableColumn<Empleado, String> columnaApellido=new TableColumn<Empleado, String>("APELLIDO");
			columnaApellido.setCellValueFactory(new PropertyValueFactory<Empleado, String>("apellido"));
			columnaApellido.setMinWidth(90);
			this.activarEditEmpleado(columnaApellido, "apellido", null);
			columnaApellido.setCellFactory(TextFieldTableCell.forTableColumn()); 
			TableColumn<Empleado, String> columnaModulos=new TableColumn<Empleado, String>("MODULOS");
			columnaModulos.setCellValueFactory(new PropertyValueFactory<Empleado, String>("modulos"));
			columnaModulos.setCellFactory(TextFieldTableCell.forTableColumn()); 
			TableColumn<Empleado, String> columnaId= new  TableColumn<Empleado, String>("ID");
			columnaId.setCellValueFactory(new PropertyValueFactory<Empleado, String>("id"));
			this.activarEditEmpleado(columnaId, "id", null);
			columnaId.setCellFactory(TextFieldTableCell.forTableColumn()); 

			tvEmpleados.getColumns().setAll(columnaNombre, columnaApellido, columnaModulos, columnaId);
			tvEmpleados.setOnDragDetected(new EventHandler<MouseEvent>(){
				public void handle(MouseEvent event) {
					Dragboard db = tvEmpleados.startDragAndDrop(TransferMode.ANY);
					ClipboardContent content = new ClipboardContent();
					content.putString(String.valueOf(tvEmpleados.getSelectionModel().getSelectedItem().getIdUsuario()));
					db.setContent(content);
					event.consume();
				}
	       	});
			tbEmpleados = new Tab("Empleados");
			tbEmpleados.setContent(tvEmpleados);
			tbEmpleados.setClosable(false);
			tbPrincipal.getTabs().add(tbEmpleados);
			tbPrincipal.getTabs().add(this.clientes());
			tbPrincipal.getTabs().add(this.herramientas());
			bp.setCenter(tbPrincipal);
			scene2.getStylesheets().add("JMetroDarkTheme.css");
			tercera.setScene(scene2);
		}
		tercera.show();
	}
	/**
		* Metodo que activa el evento para editar el TableView de tipo Empleado
		* @param columna TableColumn a activar
		* @param propiedad String con la propiedad que sera activada
		* @param tvEmpleado TableView que se editara
	*/
	public void activarEditEmpleado(TableColumn<Empleado, String> columna, String propiedad, TableView<Empleado> tvEmpleado){
		columna.setOnEditCommit(new EventHandler<CellEditEvent<Empleado, String>>(){
		@Override
			public void handle(CellEditEvent<Empleado, String> editEvent) {
					if(editEvent.getNewValue()!=(editEvent.getOldValue())){
						Empleado empleado;
						if(tvEmpleado==null)
							empleado = (Empleado)tvEmpleados.getSelectionModel().getSelectedItem();
						else
							empleado = (Empleado)tvEmpleado.getSelectionModel().getSelectedItem();
						if(propiedad.equals("nombre")){
							empleado.setNombre(editEvent.getNewValue());					
						}else if(propiedad.equals("apellido")){
							empleado.setApellido(editEvent.getNewValue());
						}else if(propiedad.equals("id")){
							empleado.setId(editEvent.getNewValue());
						}
						mngEmpleado.modificarEmpleado(empleado);
					}	
				}
			}
		);
	}
	/**
		* Metodo para cerrar el stage primario de la clase crud
	*/
	public void cerrar(){
		this.tercera.close();
	}
	/**
		* Metodo para inicializar el TableView de herramientas y retornarlo en un Tab
		* @return Retorna un Tab con el TableView de herramientas
	*/
	public Tab herramientas(){
		tvHerramientas = new TableView<Herramienta>(mngHerramienta.obtenerHerramientas());
		tvHerramientas.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tvHerramientas.setEditable(true);

		TableColumn<Herramienta, String> columnaNombre=new TableColumn<Herramienta, String>("Marca");
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Herramienta, String>("nombre"));
		columnaNombre.setMinWidth(90);
		this.activarEditHerramienta(columnaNombre, "nombre", null);
		columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Herramienta, String> columnaCategoria=new TableColumn<Herramienta, String>("Categoria");
		columnaCategoria.setCellValueFactory(new PropertyValueFactory<Herramienta, String>("categoria"));
		columnaCategoria.setMinWidth(90);
		this.activarEditHerramienta(columnaCategoria, "categoria", null);
		columnaCategoria.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Herramienta, Integer> columnaCantidad=new TableColumn<Herramienta, Integer>("Cantidad");
		columnaCantidad.setCellValueFactory(new PropertyValueFactory<Herramienta, Integer>("cantidad"));
		columnaCantidad.setMinWidth(90);

		tvHerramientas.getColumns().setAll(columnaNombre, columnaCategoria, columnaCantidad);
		tvHerramientas.setOnDragDetected(new EventHandler<MouseEvent>(){
		      	public void handle(MouseEvent event) {
				Dragboard db = tvHerramientas.startDragAndDrop(TransferMode.ANY);
				ClipboardContent content = new ClipboardContent();
				content.putString(String.valueOf(tvHerramientas.getSelectionModel().getSelectedItem().getIdHerramienta()));
				db.setContent(content);
				event.consume();
			}
		});

		tbHerramientas = new Tab("Herramientas");
		tbHerramientas.setContent(tvHerramientas);
		tbHerramientas.setClosable(false);
		return tbHerramientas;
	}
	/**
		* Metodo que activa el evento para editar el TableView de tipo Herramienta
		* @param columna TableColumn a activar
		* @param propiedad String con la propiedad que sera activada
		* @param tvHerramienta TableView que se editara
	*/
	public void activarEditHerramienta(TableColumn<Herramienta, String> columna, String propiedad, TableView<Herramienta> tvHerramienta){
		columna.setOnEditCommit(new EventHandler<CellEditEvent<Herramienta, String>>(){
		@Override
			public void handle(CellEditEvent<Herramienta, String> editEvent) {
					if(editEvent.getNewValue()!=(editEvent.getOldValue())){
						Herramienta herramienta;
						if(tvHerramienta==null)
							herramienta = (Herramienta)tvHerramientas.getSelectionModel().getSelectedItem();
						else
							herramienta = (Herramienta)tvHerramienta.getSelectionModel().getSelectedItem();
						if(propiedad.equals("nombre")){
							herramienta.setNombre(editEvent.getNewValue());					
						}else if(propiedad.equals("categoria")){
							herramienta.setCategoria(editEvent.getNewValue());
						}
						mngHerramienta.modificarHerramienta(herramienta);
					}	
				}
			}
		);
	}
	/**
		* Metodo para inicializar el TableView con los clientes y retornarlo en un Tab
		* @return Retorna un Tab con el TableView de los clientes de la base de datos
	*/
	public Tab clientes(){
		tvClientes = new TableView<Cliente>(mngCliente.obtenerClientes());
		tvClientes.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tvClientes.setEditable(true);

		TableColumn<Cliente, String> columnaNombre=new TableColumn<Cliente, String>("NOMBRE");
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nombre"));
		columnaNombre.setMinWidth(90);
		this.activarEditCliente(columnaNombre, "nombre", null);
		columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Cliente, String> columnaApellido=new TableColumn<Cliente, String>("APELLIDO");
		columnaApellido.setCellValueFactory(new PropertyValueFactory<Cliente, String>("apellido"));
		this.activarEditCliente(columnaApellido, "apellido", null);
		columnaApellido.setCellFactory(TextFieldTableCell.forTableColumn()); 
		TableColumn<Cliente, String> columnaCarne=new TableColumn<Cliente, String>("Carne");
		columnaCarne.setCellValueFactory(new PropertyValueFactory<Cliente, String>("carne"));
		this.activarEditCliente(columnaCarne, "carne", null);
		columnaCarne.setCellFactory(TextFieldTableCell.forTableColumn()); 
		TableColumn<Cliente, String> columnaSeccion= new  TableColumn<Cliente, String>("Seccion");
		columnaSeccion.setCellValueFactory(new PropertyValueFactory<Cliente, String>("seccion"));
		this.activarEditCliente(columnaSeccion, "seccion", null);
		columnaSeccion.setCellFactory(TextFieldTableCell.forTableColumn()); 
		TableColumn<Cliente, String> columnaJornada = new TableColumn<Cliente, String>("Jornada");
		columnaJornada.setCellValueFactory(new PropertyValueFactory<Cliente, String>("jornada"));
		this.activarEditCliente(columnaJornada, "jornada", null);
		columnaJornada.setCellFactory(TextFieldTableCell.forTableColumn()); 

		tvClientes.getColumns().setAll(columnaNombre, columnaApellido, columnaCarne, columnaSeccion, columnaJornada);
		tvClientes.setOnDragDetected(new EventHandler<MouseEvent>(){
			public void handle(MouseEvent event){
				Dragboard db = tvClientes.startDragAndDrop(TransferMode.ANY);
				ClipboardContent content = new ClipboardContent();
				content.putString(String.valueOf(tvClientes.getSelectionModel().getSelectedItem().getIdUsuario()));
				db.setContent(content);
				event.consume();
			}
		});

		tbClientes = new Tab("Clientes");
		tbClientes.setContent(tvClientes);
		tbClientes.setClosable(false);
		return tbClientes;
	}
	/**
		* Metodo que activa el evento para editar el TableView de tipo Cliente
		* @param columna TableColumn a activar
		* @param propiedad String con la propiedad que sera activada
		* @param tvCliente TableView que se editara
	*/
	public void activarEditCliente(TableColumn<Cliente, String> columna, String propiedad, TableView<Cliente> tvCliente){
		columna.setOnEditCommit(new EventHandler<CellEditEvent<Cliente, String>>(){
		@Override
			public void handle(CellEditEvent<Cliente, String> editEvent) {
					if(editEvent.getNewValue()!=(editEvent.getOldValue())){
						Cliente cliente;
						if(tvCliente==null)
							cliente = (Cliente)tvClientes.getSelectionModel().getSelectedItem();
						else
							cliente = (Cliente)tvCliente.getSelectionModel().getSelectedItem();
						if(propiedad.equals("nombre")){
							cliente.setNombre(editEvent.getNewValue());					
						}else if(propiedad.equals("apellido")){
							cliente.setApellido(editEvent.getNewValue());
						}else if(propiedad.equals("carne")){
							cliente.setCarne(editEvent.getNewValue());
						}else if(propiedad.equals("seccion")){
							cliente.setSeccion(editEvent.getNewValue());
						}else if(propiedad.equals("jornada")){
							cliente.setJornada(editEvent.getNewValue());
						}
						mngCliente.modificarCliente(cliente);
					}	
				}
			}
		);
	}
	/**
		* Metodo que verifica que los TextFiel del login no estan vacios
		* @param user String con el id del usuario
		* @param pass String con el passwor del usuario
		* @return Retorna un boolean respectivo a si ambos estan vacios o no
	*/
	public boolean verificarDatos(String user, String pass){
		if(user.trim().equals("") && pass.trim().equals(""))
			return true;
		return false;
	}
}
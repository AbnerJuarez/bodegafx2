package org.abnerjuarez.recursos;

import org.abnerjuarez.manejadores.ManejadorEmpleado;
import org.abnerjuarez.manejadores.ManejadorCliente;
import org.abnerjuarez.manejadores.ManejadorHerramienta;
import org.abnerjuarez.beans.Cliente;
import org.abnerjuarez.beans.Empleado;
import org.abnerjuarez.beans.Herramienta;
import org.abnerjuarez.recursos.DragDrop;
import org.abnerjuarez.visual.Crud;

import javafx.collections.ObservableList;
import javafx.application.Application;
import javafx.event.Event;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.Node;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TextField;
import java.util.ArrayList;
import javafx.scene.input.*;

/**
	* Clase encargada de las busqueda en el modulo Crud
	* @author Abner A. Juarez D.
*/
public class Buscador extends Application implements EventHandler<Event>{
	//Propiedades
	private GridPane gpParametros;
	private Scene scnPrincipal;
	private TextField tfBusqueda;
	private Stage stgPrincipal;
	private Button btBuscar;
	private BorderPane bpBusqueda;
	private ManejadorCliente mngCliente;
	private ManejadorEmpleado mngEmpleado;
	private ManejadorHerramienta mngHerramienta;
	private String tipoBusqueda;
	private DragDrop dragDrop;
	private Crud crud;
	/**
		* Constructor de la clase con un solo parametro
		* @param me ManejadorEmpleado a inyectar en la clase
	*/
	public Buscador(ManejadorEmpleado me){
		this.mngEmpleado=me;
		this.tipoBusqueda="Empleado";
	}
	/**
		* Constructor de la clase con un solo parametro
		* @param mh ManejadorHerramienta a inyectar en la clase
	*/
	public Buscador(ManejadorHerramienta mh){
		this.mngHerramienta=mh;
		this.tipoBusqueda="Herramienta";
	}
	/**
		* Constructor de la clase con un solo parametro
		* @param mc ManejadorCliente a inyectar en la clase
	*/
	public Buscador(ManejadorCliente mc){
		this.mngCliente=mc;
		this.tipoBusqueda="Cliente";
	}
	/**
		* Constructor de la clase con todos los parametros
		* @param me ManejadorEmpleado a inyectar en la clase
		* @param mh ManejadorHerramienta a inyectar en la clase
		* @param mc ManejadorCliente a inyectar en la clase
	*/
	public Buscador(ManejadorCliente mc, ManejadorHerramienta mh, ManejadorEmpleado me){
		this.mngCliente=mc;
		this.mngHerramienta=mh;
		this.mngEmpleado=me;
	}
	/**
		* Metodo para inyectar la instancia de la clase Crud
		* @param crud Crud a inyectar
	*/
	public void setCrud(Crud crud){
		this.crud=crud;
	}
	/**
		* Metodo para indicar el tipo de busqueda que se hará
		* @param tipo String con el valor del tipo de busqueda
	*/
	public void setTipo(String tipo){
		this.tipoBusqueda=tipo;
	}
	/**
		* Metodo de inicio heredado de Application
		* @param primary Stage que carga la ventana primaria
	*/
	public void start(Stage primary){
	}
	/**
		* Metodo encargado de manejar los eventos de la clase
		* @param event Event generico para ser manejado
	*/
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource().equals(btBuscar))
				this.eventoBuscar();
		}else if(event instanceof KeyEvent){
			if(((KeyEvent)event).getCode() == KeyCode.ENTER){
				if(event.getSource().equals(tfBusqueda))
					this.eventoBuscar();
			}
		}
	}
	/**
		* Metodo que distingue el tipo de busqueda
	*/
	public void eventoBuscar(){
		stgPrincipal.setMinWidth(500);
		stgPrincipal.setMinHeight(500);
		if(tipoBusqueda.equals("Empleado")){
			bpBusqueda.setCenter(tablaEmpleados());
		}else if(tipoBusqueda.equals("Herramienta")){
			bpBusqueda.setCenter(tablaHerramientas());
		}else if(tipoBusqueda.equals("Cliente")){
			bpBusqueda.setCenter(tablaClientes());
		}
		tfBusqueda.setText("");
	}
	/**
		* Metodo que inicia la clase Buscador
	*/
	public void iniciar(){
		this.gpParametros = new GridPane();
		this.stgPrincipal = new Stage();
		this.tfBusqueda = new TextField();
		this.tfBusqueda.addEventHandler(KeyEvent.KEY_PRESSED, this);
		this.bpBusqueda = new BorderPane();
		this.btBuscar = new Button("Buscar");
		this.btBuscar.addEventHandler(ActionEvent.ACTION, this);
		this.tfBusqueda.setPromptText("Parametro");
		gpParametros.add(new Label("Buscar: "), 0, 0);
		gpParametros.add(tfBusqueda, 1, 0);
		gpParametros.add(btBuscar, 1, 2);
		bpBusqueda.setTop(gpParametros);
		this.scnPrincipal = new Scene(bpBusqueda);
		this.scnPrincipal.getStylesheets().add("JMetroDarkTheme.css");
		this.stgPrincipal.setTitle("Busqueda Avanzada");
		this.stgPrincipal.setScene(scnPrincipal);
		this.stgPrincipal.show();
	}
	/**
		* Metodo que carga los clientes encontrados en TableView
		* @return Retorna un TableView con los clientes de la busqueda
	*/
	public TableView<Cliente> tablaClientes(){
		TableView<Cliente> tvClientes = new TableView<Cliente>(mngCliente.buscarClienteAvanzada(tfBusqueda.getText()));
		tvClientes.setEditable(true);

		TableColumn<Cliente, String> columnaNombre=new TableColumn<Cliente, String>("NOMBRE");
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Cliente, String>("nombre"));
		columnaNombre.setMinWidth(90);
		crud.activarEditCliente(columnaNombre, "nombre", tvClientes);
		columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn()); 
		TableColumn<Cliente, String> columnaApellido=new TableColumn<Cliente, String>("APELLIDO");
		columnaApellido.setCellValueFactory(new PropertyValueFactory<Cliente, String>("apellido"));
		crud.activarEditCliente(columnaApellido, "apellido", tvClientes);
		columnaApellido.setCellFactory(TextFieldTableCell.forTableColumn()); 
		TableColumn<Cliente, String> columnaCarne=new TableColumn<Cliente, String>("Carne");
		columnaCarne.setCellValueFactory(new PropertyValueFactory<Cliente, String>("carne"));
		crud.activarEditCliente(columnaCarne, "carne", tvClientes);
		columnaCarne.setCellFactory(TextFieldTableCell.forTableColumn()); 
		TableColumn<Cliente, String> columnaSeccion= new  TableColumn<Cliente, String>("Seccion");
		columnaSeccion.setCellValueFactory(new PropertyValueFactory<Cliente, String>("seccion"));
		crud.activarEditCliente(columnaSeccion, "seccion", tvClientes);
		columnaSeccion.setCellFactory(TextFieldTableCell.forTableColumn()); 
		TableColumn<Cliente, String> columnaJornada = new TableColumn<Cliente, String>("Jornada");
		columnaJornada.setCellValueFactory(new PropertyValueFactory<Cliente, String>("jornada"));
		crud.activarEditCliente(columnaJornada, "jornada", tvClientes);
		columnaJornada.setCellFactory(TextFieldTableCell.forTableColumn());

		tvClientes.getColumns().setAll(columnaNombre, columnaApellido, columnaCarne, columnaSeccion, columnaJornada);
		tvClientes.setOnDragDetected(new EventHandler<MouseEvent>(){
			public void handle(MouseEvent event){
				Dragboard db = tvClientes.startDragAndDrop(TransferMode.ANY);
				ClipboardContent content = new ClipboardContent();
				content.putString(String.valueOf(tvClientes.getSelectionModel().getSelectedItem().getIdUsuario()));
				db.setContent(content);
				event.consume();
			}
		});

		return tvClientes;
	}
	/**
		* Metodo que carga los empleados encontrados en un TableView
		* @return Retorna un TableView con los empleados encontrados
	*/
	public TableView<Empleado> tablaEmpleados(){
		ObservableList<Empleado> empleados2 = FXCollections.observableArrayList(mngEmpleado.buscarEmpleadoAvanzada(tfBusqueda.getText()));
		TableView<Empleado> tvEmpleados = new TableView<Empleado>(empleados2);
		tvEmpleados.setEditable(true);

		TableColumn<Empleado, String> columnaNombre=new TableColumn<Empleado, String>("NOMBRE");
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Empleado, String>("nombre"));
		columnaNombre.setMinWidth(90);
		crud.activarEditEmpleado(columnaNombre, "nombre", tvEmpleados);
		columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Empleado, String> columnaApellido=new TableColumn<Empleado, String>("APELLIDO");
		columnaApellido.setCellValueFactory(new PropertyValueFactory<Empleado, String>("apellido"));
		columnaApellido.setMinWidth(90);
		crud.activarEditEmpleado(columnaApellido, "apellido", tvEmpleados);
		columnaApellido.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Empleado, String> columnaModulos=new TableColumn<Empleado, String>("MODULOS");
		columnaModulos.setCellValueFactory(new PropertyValueFactory<Empleado, String>("modulos"));
		columnaModulos.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Empleado, String> columnaId= new  TableColumn<Empleado, String>("ID");
		columnaId.setCellValueFactory(new PropertyValueFactory<Empleado, String>("id"));
		columnaId.setCellFactory(TextFieldTableCell.forTableColumn());
		crud.activarEditEmpleado(columnaId, "id", tvEmpleados);

		tvEmpleados.getColumns().setAll(columnaNombre, columnaApellido, columnaModulos, columnaId);
		tvEmpleados.setOnDragDetected(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent event) {
                Dragboard db = tvEmpleados.startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();
                content.putString(String.valueOf(tvEmpleados.getSelectionModel().getSelectedItem().getIdUsuario()));
                db.setContent(content);
                event.consume();
            }
        });
		
		return tvEmpleados;
	}
	/**
		* Metodo que carga las herramientas encontradas en un TableView
		* @return Retorna un TableView con las herramientas encontradas
	*/
	public TableView<Herramienta> tablaHerramientas(){
		ObservableList<Herramienta> herramientas2 = FXCollections.observableArrayList(mngHerramienta.buscarHerramientaAvanzada(tfBusqueda.getText()));
		TableView<Herramienta> tvHerramientas = new TableView<Herramienta>(herramientas2);
		tvHerramientas.setEditable(true);

		TableColumn<Herramienta, String> columnaNombre=new TableColumn<Herramienta, String>("Marca");
		columnaNombre.setCellValueFactory(new PropertyValueFactory<Herramienta, String>("nombre"));
		columnaNombre.setMinWidth(90);
		crud.activarEditHerramienta(columnaNombre, "nombre", tvHerramientas);
		columnaNombre.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Herramienta, String> columnaCategoria=new TableColumn<Herramienta, String>("Categoria");
		columnaCategoria.setCellValueFactory(new PropertyValueFactory<Herramienta, String>("categoria"));
		columnaCategoria.setMinWidth(90);
		crud.activarEditHerramienta(columnaCategoria, "categoria", tvHerramientas);
		columnaCategoria.setCellFactory(TextFieldTableCell.forTableColumn());
		TableColumn<Herramienta, Integer> columnaCantidad=new TableColumn<Herramienta, Integer>("Cantidad");
		columnaCantidad.setCellValueFactory(new PropertyValueFactory<Herramienta, Integer>("cantidad"));
		columnaCantidad.setMinWidth(90);

		tvHerramientas.getColumns().setAll(columnaNombre, columnaCategoria, columnaCantidad);
		tvHerramientas.setOnDragDetected(new EventHandler<MouseEvent>(){
            public void handle(MouseEvent event) {
                Dragboard db = tvHerramientas.startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();
                content.putString(String.valueOf(tvHerramientas.getSelectionModel().getSelectedItem().getIdHerramienta()));
                db.setContent(content);
                event.consume();
            }
        });
		
		return tvHerramientas;
	}
}
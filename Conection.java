package org.abnerjuarez.db;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

/**
	* Clase que administra el driver de la conexion a la base de datos
	* @author Inguelberth E. Garcia M.
*/
public class Conection{
	//Propiedades
	private Connection connection;
	private Statement state;
	private File conf;
	private FileReader fileReader;
	private BufferedReader reader;
	private ArrayList<String> data;
	/**
		* Constructor vacio de la clase Conection donde se carga el drive JDBC
	*/
	public Conection(){
		try{
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			this.readConnection();

			connection = DriverManager.getConnection("jdbc:sqlserver://"+data.get(0)+"\\"+data.get(1)+";databasename="+data.get(2)+"", data.get(3), data.get(4));
			state = connection.createStatement();
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}catch(ClassNotFoundException classn){
			System.out.println("No se ha encontrado el driver");
		}
	}
	/**
		* Clase que carga la configuracion del archivo .conf
	*/
	public void readConnection(){
		try{
			conf = new File("ServerConnection.conf");
			fileReader = new FileReader(conf);
			reader = new BufferedReader(fileReader);
			this.data = new ArrayList<String>();
			for(int i=0; i<5; i++){
				String[] dataArray = reader.readLine().split("=");
				this.data.add(dataArray[1]);
			}
		}catch(IOException errorLeerDatos ){
			System.out.println("Error al leer datos");
		}
	}
	/**
		* Metodo para ejecutar una sentencia en la base de datos
		* @param sentencia String con las instrucciones de la sentencia que se ejecutara
	*/
	public void ejecutarSentencia(String sentencia){
		try{
			state.execute(sentencia);
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	/**
		* Metodo para ejecutar una consulta a la base de datos
		* @param consulta String con las instrucciones de la consulta que se ejecutara
		* @return Retorna un ResultSet con los resultado de la consulta
	*/
	public ResultSet ejecutarConsulta(String consulta){
		ResultSet result = null;
		try{
			result = state.executeQuery(consulta);
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return result;
	}
}


package org.abnerjuarez.recursos;

import org.abnerjuarez.manejadores.ManejadorHerramienta;
import org.abnerjuarez.manejadores.ManejadorPrestamo;
import org.abnerjuarez.manejadores.ManejadorEmpleado;
import org.abnerjuarez.manejadores.ManejadorCliente;
import org.abnerjuarez.beans.Prestamo;
import org.abnerjuarez.beans.Herramienta;
import org.abnerjuarez.beans.Cliente;
import org.abnerjuarez.beans.Empleado;
import org.abnerjuarez.beans.DetallePrestamo;
import org.abnerjuarez.db.Conection;

import javafx.collections.ObservableList;
import java.util.ArrayList;

/**
	* Clase encargada de eliminar las entidades de la base de datos
	* @author Abner A. Juarez D.
*/
public class Eliminador{
	//Propiedades
	private ManejadorCliente mngCliente;
	private ManejadorEmpleado mngEmpleado;
	private ManejadorPrestamo mngPrestamo;
	private ManejadorHerramienta mngHerramienta;
	/**
		* Constructor de la clase con todos los parametros
		* @param me ManejadorEmpleado a inyectar en la clase
		* @param mh ManejadorHerramienta a inyectar en la clase
		* @param mc ManejadorCliente a inyectar en la clase
		* @param mp ManejadorPrestamo a inyectar en la clase
	*/
	public Eliminador(ManejadorHerramienta mh, ManejadorPrestamo mp, ManejadorEmpleado me, ManejadorCliente mc){
		this.mngCliente=mc;
		this.mngHerramienta=mh;
		this.mngPrestamo=mp;
		this.mngEmpleado=me;
	}
	/**
		* Constructor de la clase con el ManejadorPrestamo como unico parametro
		* @param mp ManejadorPrestamo a inyectar en la clase
	*/
	public Eliminador(ManejadorPrestamo mp){
		this.mngPrestamo=mp;
		this.mngHerramienta = new ManejadorHerramienta(new Conection());
	}
	/**
		* Metodo encargado de eliminar los detalles de un prestamo
		* @param idPrestamo Int con el valor del idPrestamo del que se eliminaran los detalles
	*/
	public void eliminarDetalles(int idPrestamo){
		ObservableList<DetallePrestamo> detalles=mngPrestamo.obtenerDetalles(idPrestamo);
		ArrayList<DetallePrestamo> detallesArray=new ArrayList<DetallePrestamo>(detalles);
		for(DetallePrestamo detalle:detallesArray){
			mngPrestamo.eliminarDetalle(detalle);
		}
		mngPrestamo.actualizarPrestamos();
		mngHerramienta.actualizarHerramientas();

	}
	/**
		* Metodo encargado de eliminar un empleado del programa
		* @param idEmpleado Int con el valor del idEmpleado que se eliminara
	*/
	public void eliminarEmpleado(int idEmpleado){
		Empleado empleado=mngEmpleado.buscarEmpleado(idEmpleado);
		ObservableList<Prestamo> prestamos=mngPrestamo.obtenerPrestamos();
		ArrayList<Prestamo> prestamosArray = new ArrayList<Prestamo>();
		for(Prestamo prestamo:prestamos){
			if(prestamo.getIdEmpleado()==idEmpleado)
				prestamosArray.add(prestamo);
		}
		for(Prestamo prestamo2:prestamosArray){
			this.eliminarDetalles(prestamo2.getIdPrestamo());
			mngPrestamo.eliminarPrestamo(prestamo2);
		}
		mngEmpleado.eliminarEmpleado(empleado);
	}
	/**
		* Metodo encargado de eliminar un cliente del programa
		* @param idCliente Int con el valor del idCliente que se eliminara
	*/
	public void eliminarCliente(int idCliente){
		Cliente cliente = mngCliente.buscarCliente(idCliente);
		ObservableList<Prestamo> prestamos=mngPrestamo.obtenerPrestamos();
		ArrayList<Prestamo> prestamosArray = new ArrayList<Prestamo>();
		for(Prestamo prestamo:prestamos){
			if(prestamo.getIdCliente()==idCliente)
				prestamosArray.add(prestamo);
		}
		for(Prestamo prestamo2:prestamosArray){
			this.eliminarDetalles(prestamo2.getIdPrestamo());
			mngPrestamo.eliminarPrestamo(prestamo2);
		}
		mngCliente.eliminarCliente(cliente);
	}
	/**
		* Metodo encargado de eliminar un herramienta del programa
		* @param idHerramienta Int con el valor del idHerramienta que se eliminara
	*/
	public void eliminarHerramienta(int idHerramienta){
		Herramienta herramienta = mngHerramienta.buscarHerramienta(idHerramienta);
		ObservableList<Prestamo> prestamos=mngPrestamo.obtenerPrestamos();
		ArrayList<Prestamo> prestamosArray = new ArrayList<Prestamo>();
		for(Prestamo prestamo:prestamos){
			for(DetallePrestamo detallep:mngPrestamo.obtenerDetalles(prestamo.getIdPrestamo())){
				if(detallep.getIdHerramienta()==idHerramienta)
					prestamosArray.add(prestamo);
			}
		}
		for(Prestamo prestamo2:prestamosArray){
			this.eliminarDetalles(prestamo2.getIdPrestamo());
			mngPrestamo.eliminarPrestamo(prestamo2);
		}
		mngHerramienta.eliminarHerramienta(herramienta);
	}
}
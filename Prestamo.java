package org.abnerjuarez.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
	* Clase tipo beans con todas las propiedades que debe llevar un prestamo
	* @author Abner A. Juarez D.
*/
public class Prestamo{
	//Propiedades
	private IntegerProperty idPrestamo, idCliente, idEmpleado, cantidad;
	private StringProperty fecha, fechaEntrega, estado, nombreCliente, nombreEmpleado;
	/**
		* Metodo para instanciar las propiedades de la clase
	*/
	public void instanciar(){
		idEmpleado = new SimpleIntegerProperty();
		idPrestamo = new SimpleIntegerProperty();
		idCliente = new SimpleIntegerProperty();
		cantidad = new SimpleIntegerProperty();
		fecha = new SimpleStringProperty();
		fechaEntrega = new SimpleStringProperty();
		estado = new SimpleStringProperty();
		nombreCliente = new SimpleStringProperty();
		nombreEmpleado = new SimpleStringProperty();
	}
	/**
		* Constructor vacio de la clase Prestamo
	*/
	public Prestamo(){
		this.instanciar();
	}
	/**
		* Constructor con todos los parametros para crear el objeto
		* @param idPrestamo Int con el valor del idPrestamo
		* @param idCliente Int con el valor del idCliente
		* @param nombreCliente String con el valor del nombre del cliente
		* @param nombreEmpleado String con el valor del nombre del empleado
		* @param cantidad Int con el valor de la cantidad de herramientas en el prestamo
		* @param idEmpleado Int con el valor del idEmpleado
		* @param fecha String con el valor de la fecha
		* @param fechaEntrega Strinc con el valor de la fecha para entregar
		* @param estado String con el estado del prestamo
	*/
	public Prestamo(int idPrestamo, int idCliente, String nombreCliente, int idEmpleado, String nombreEmpleado, String fecha, String fechaEntrega, int cantidad, String estado){
		this.instanciar();
		this.setidPrestamo(idPrestamo);
		this.setIdCliente(idCliente);
		this.setNombreCliente(nombreCliente);
		this.setIdEmpleado(idEmpleado);
		this.setNombreEmpleado(nombreEmpleado);
		this.setFecha(fecha);
		this.setFechaEntrega(fechaEntrega);
		this.setCantidad(cantidad);
		this.setEstado(estado);
	}
	/**
		* Metodo para setter el nombre del Empleado
		* @param nombreEmpleado Strinc con el nombre del empleado
	*/
	public void setNombreEmpleado(String nombreEmpleado){
		this.nombreEmpleado.set(nombreEmpleado);
	}
	/**
		* Settea el nombre del cliente 
		* @param nombreCliente String con el nombre del cliente
	*/
	public void setNombreCliente(String nombreCliente){
		this.nombreCliente.set(nombreCliente);
	}
	/**
		* Settea el estado del prestamo
		* @param estado String con el estado del prestamo
	*/
	public void setEstado(String estado){
		this.estado.set(estado);
	}
	/**
		* Settea la fecha de entrega del prestamo
		* @param fechaEntrega String con la fecha de entrega del prestamo
	*/
	public void setFechaEntrega(String fechaEntrega){
		this.fechaEntrega.set(fechaEntrega);
	}
	/**
		* Settea el idPrestamo del prestamo
		* @param idPrestamo Int con el idPrestamo del prestamo
	*/
	public void setidPrestamo(int idPrestamo){
		this.idPrestamo.set(idPrestamo);
	}
	/**
		* Settea el idCliente del prestamo
		* @param idCliente Int con el idCliente del prestamo
	*/
	public void setIdCliente(int idCliente){
		this.idCliente.set(idCliente);
	}
	/**
		* Settea el idEmpleado del prestamo
		* @param idEmpleado Int con el idEmpleado del prestamo
	*/
	public void setIdEmpleado(int idEmpleado){
		this.idEmpleado.set(idEmpleado);
	}
	/**
		* Settea la fecha del prestamo
		* @param fecha String con la fecha del prestamo
	*/
	public void setFecha(String fecha){
		this.fecha.set(fecha);
	}
	/**
		* Settea la cantidad de herramientas prestada
		* @param cantidad Int con el valor de la cantidad del prestamo
	*/
	public void setCantidad(int cantidad){
		this.cantidad.set(cantidad);
	}
	/**
		* Obtiene el valor del estado del prestamo
		* @return Retorna un String con el estado
	*/
	public String getEstado(){
		return this.estado.get();
	}
	/**
		* Obtiene el nombre del cliente del prestamo
		* @return Retorna un String con el nombre
	*/
	public String getNombreCliente(){
		return this.nombreCliente.get();
	}
	/**
		* Obtiene el nombre del empleado del prestamo
		* @return Retorna un String con el nombre del empleado
	*/
	public String getNombreEmpleado(){
		return this.nombreEmpleado.get();
	}
	/**
		* Obtiene la fecha de entrega del prestamo
		* @return Retorna un String con la fecha de entrega
	*/
	public String getFechaEntrega(){
		return this.fechaEntrega.get();
	}
	/**
		* Obtiene el idPrestamo del prestamo
		* @return Retorna un int con el idPrestamo
	*/
	public int getIdPrestamo(){
		return this.idPrestamo.get();
	}
	/**
		* Obtiene el idCliente del prestamo
		* @return Retorna un int con el idCliente
	*/
	public int getIdCliente(){
		return this.idCliente.get();
	}
	/**
		* Obtiene el idEmpleado del prestamo 
		* @return Retorna un int con el idEmpleado
	*/
	public int getIdEmpleado(){
		return this.idEmpleado.get();
	}
	/**
		* Obtiene la fecha del prestamo 
		* @return Retorna un String con la fecha
	*/
	public String getFecha(){
		return this.fecha.get();
	}
	/**
		* Obtiene la cantidad del prestamo
		* @return Retorna un int con la cantidad
	*/
	public int getCantidad(){
		return this.cantidad.get();
	}
}
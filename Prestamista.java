package org.abnerjuarez.visual;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.GridPane;
import javafx.geometry.Orientation;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TextField;
import javafx.collections.FXCollections;
import javafx.stage.Modality;

import java.util.ArrayList;
import java.util.Locale;

import org.abnerjuarez.beans.Prestamo;
import org.abnerjuarez.beans.Cliente;
import org.abnerjuarez.beans.Herramienta;
import org.abnerjuarez.beans.DetallePrestamo;
import org.abnerjuarez.beans.Empleado;
import org.abnerjuarez.visual.Crud;
import org.abnerjuarez.recursos.Eliminador;
import org.abnerjuarez.calendar.DatePicker;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.abnerjuarez.manejadores.ManejadorCliente;
import org.abnerjuarez.manejadores.ManejadorEmpleado;
import org.abnerjuarez.manejadores.ManejadorHerramienta;
import org.abnerjuarez.manejadores.ManejadorPrestamo;
import org.abnerjuarez.db.Conection;

/**
	* Clase encargada de administrar el modulo Prestamista del programa
	* @author Abner A. Juarez D.
*/
public class Prestamista extends Application implements EventHandler<Event>{
	//Prestamista
	private ManejadorHerramienta mngHerramienta;
	private ManejadorEmpleado mngEmpleado;
	private ManejadorCliente mngCliente;
	private ManejadorPrestamo mngPrestamo;
	private Eliminador eliminador;
	private Button btEliminar, btAgregarc, btAgregard, btEliminard, btAgregarp;
	private TextField tfIdCliente, nHerramientad, cantidadd; 
	private DatePicker tfFechaEntrega;
	private ToolBar tbDetalle, tbPrestamo;
	private GridPane crudPrestamo, gpAgregard;
	private TableView tvPrestamos, tvDetalles;
	private VBox vbDetallePrestamos;
	private Stage stgPrincipal, stgAgregard;
	private SplitMenuButton smbHerramienta;
	private Scene scnPrincipal, scnAgregard;
	private Conection conect;
	private SplitPane spHorizontal, spVertical;
	private BorderPane bpDetalle, bpPrestamo;
	private ResultSet result;
	private int idPrestamoAll, prestamoDetalle;
	private ObservableList<Herramienta> listHerramientas;
	/**
		* Metodo para iniciar la clase heredado de Application
		* @param primary Stage que carga la ventana primaria de la clase
	*/
	public void start(Stage primary){
	}
	/**
		* Metodo para cerrar el Stage principal de la clase
	*/
	public void cerrar(){
		this.stgPrincipal.close();
	}
	/**
		* Metodo que inicializa la clase
	*/
	public void iniciar(){
		this.stgPrincipal = new Stage();
		this.eliminador = new Eliminador(mngHerramienta, mngPrestamo, mngEmpleado, mngCliente);
		this.bpDetalle = new BorderPane();
		this.btAgregarp = new Button("Agregar Prestamo");
		this.btAgregarp.addEventHandler(ActionEvent.ACTION, this);
		this.bpPrestamo = new BorderPane();
		this.crudPrestamo = new GridPane();
		this.crudPrestamo.setVgap(5);
		this.vbDetallePrestamos = new VBox();
		this.btEliminar = new Button("Eliminar");
		this.btEliminar.setId("btEliminar");
		this.btEliminar.addEventHandler(ActionEvent.ACTION, this);
		this.btAgregarc = new Button("Agregar Cliente");
		this.btAgregarc.setId("btAgregarc");
		this.btAgregarc.addEventHandler(ActionEvent.ACTION, this);
		this.btEliminard = new Button("Eliminar");
		this.btEliminard.setId("btEliminard");
		this.btEliminard.addEventHandler(ActionEvent.ACTION, this);
		this.btAgregard = new Button("Agregar");
		this.btAgregard.setId("btAgregard");
		this.btAgregard.addEventHandler(ActionEvent.ACTION, this);
		this.spHorizontal = new SplitPane();
		this.spVertical = new SplitPane();
		this.tbDetalle = new ToolBar();
		this.tbPrestamo = new ToolBar();
		spVertical.setOrientation(Orientation.VERTICAL);
		tfFechaEntrega = new DatePicker();
		tfFechaEntrega.localeProperty().set(Locale.US);
		tfFechaEntrega.setLocale(Locale.UK);
		tfFechaEntrega.getCalendarView().setShowWeeks(false);
		tfFechaEntrega.getCalendarView().todayButtonTextProperty().set("Hoy");
		tfFechaEntrega.addEventHandler(KeyEvent.KEY_PRESSED, this);
		tfFechaEntrega.getTextField().setTooltip(new Tooltip("DoubleClick para mostrar calendario"));
		tfIdCliente = new TextField();
		tfIdCliente.setPromptText("DPI Cliente");
		tfIdCliente.addEventHandler(KeyEvent.KEY_PRESSED, this);

		tbPrestamo.getItems().addAll(btEliminar, btAgregarc);
		bpPrestamo.setBottom(tbPrestamo);
		crudPrestamo.add(new Label("DPI Cliente"), 0, 0);
		crudPrestamo.add(tfIdCliente, 1, 0);
		crudPrestamo.add(new Label("Fecha de Entrega"), 0, 1);
		crudPrestamo.add(tfFechaEntrega, 1, 1);
		crudPrestamo.add(btAgregarp, 1, 2);
		bpPrestamo.setCenter(crudPrestamo);
		tbDetalle.getItems().addAll(btAgregard, btEliminard);
		bpDetalle.setTop(tbDetalle);
		spHorizontal.getItems().addAll(bpPrestamo, bpDetalle);
		spVertical.getItems().addAll(spHorizontal, this.getPrestamos());

		this.mngPrestamo.actualizarOutEntregate();
		this.mngPrestamo.actualizarPrestamos();
		scnPrincipal = new Scene(spVertical);
		scnPrincipal.getStylesheets().add("prestamista.css");
		stgPrincipal.setTitle("Prestamista");
		stgPrincipal.setScene(scnPrincipal);
		stgPrincipal.show();
		this.mngPrestamo.actualizarOutEntregate();
	}
	public void eventoAgregar(){
		if(this.obtenerVacio(crudPrestamo)!=null){
			TextField vacio=this.obtenerVacio(crudPrestamo);
			vacio.requestFocus();
		}else{
			try{
				Cliente cliente = mngCliente.buscarCliente(tfIdCliente.getText());
				if(cliente != null){
					if(mngPrestamo.verificarFecha(mngPrestamo.formatearFecha(tfFechaEntrega.selectedDateProperty().get()))){
						Empleado conectado=mngEmpleado.getEmpleadoConectado();
						mngPrestamo.insertarPrestamo(cliente.getIdUsuario(), conectado.getIdUsuario(), mngPrestamo.formatearFecha(tfFechaEntrega.selectedDateProperty().get()), 0, "Pendiente");
						this.spVertical.getItems().clear();
						this.spVertical.getItems().addAll(spHorizontal, this.getPrestamos());
						tfFechaEntrega.setText("");
						tfIdCliente.setText("");
						this.prestamoDetalle=mngPrestamo.obtenerOutEntregate().get((mngPrestamo.obtenerOutEntregate().size())-1).getIdPrestamo();
						this.idPrestamoAll=0;
					}
				}
			}catch(NullPointerException npe){
				npe.printStackTrace();
			}
		}
	}
	/**
		* Metodo encargado de manejar los eventos de la clase
		* @param event Event con el evento que se manejara
	*/
	public void handle(Event event){
		Prestamo prestamo = (Prestamo)tvPrestamos.getSelectionModel().getSelectedItem();
		if(event instanceof KeyEvent){
			KeyEvent kevent=(KeyEvent)event;
			if(kevent.getCode() == KeyCode.ENTER){
				if(event.getSource().equals(tfFechaEntrega) || event.getSource().equals(tfIdCliente)){
					if(mngCliente.buscarCarne(tfIdCliente.getText())){
						this.eventoAgregar();
						this.agregarDetalle();
					}
				}else if(event.getSource().equals(cantidadd)){
					if(this.obtenerVacio(gpAgregard)!=null){
						TextField vacio=this.obtenerVacio(gpAgregard);
						vacio.requestFocus();
					}else{
						Herramienta herramienta=mngHerramienta.buscarHerramienta(smbHerramienta.getText());
						if(herramienta!=null){
							if(herramienta.getCantidad()>0){
								try{
									int cantidad = Integer.parseInt(cantidadd.getText());
									if(idPrestamoAll!=0){
										mngPrestamo.insertarDetalle(idPrestamoAll, herramienta.getIdHerramienta(), cantidad);
									}else if(idPrestamoAll==0){
										mngPrestamo.insertarDetalle(prestamoDetalle, herramienta.getIdHerramienta(), cantidad);
									}
									cantidadd.setText("");
									mngHerramienta.actualizarHerramientas();
									this.spVertical.getItems().clear();
									this.spVertical.getItems().addAll(spHorizontal, this.getPrestamos());
									idPrestamoAll=0;
									this.prestamoDetalle=0;
									stgAgregard.close();
								}catch(NullPointerException e){
									e.printStackTrace();
								}catch(NumberFormatException nfe){
									nfe.printStackTrace();
								}
							}
						}
					}
				}
			}
		}else if(event instanceof MouseEvent){
			if(event.getSource().equals(tvPrestamos)){
				try{
					idPrestamoAll=prestamo.getIdPrestamo();
					bpDetalle.setCenter(null);
					bpDetalle.setCenter(this.getDetalle(((Prestamo)tvPrestamos.getSelectionModel().getSelectedItem()).getIdPrestamo()));
				}catch(NullPointerException e){
					//e.printStackTrace();
				}
			}
		}else if(event instanceof ActionEvent){
			if(event.getSource().equals(btAgregard)){
				this.agregarDetalle();
			} else if(event.getSource().equals(btEliminar)){
				ObservableList<Prestamo> prestamoSeleccionados = tvPrestamos.getSelectionModel().getSelectedItems();
				ArrayList<Prestamo> listPrestamo = new ArrayList<Prestamo>(prestamoSeleccionados);			
				for(Prestamo prestamoe : listPrestamo){
					eliminador.eliminarDetalles(prestamoe.getIdPrestamo());
					mngPrestamo.eliminarPrestamo(prestamoe);
				}
				mngPrestamo.actualizarOutEntregate();
				mngHerramienta.actualizarHerramientas();
			} else if(event.getSource().equals(btEliminard)){
				try{
					mngPrestamo.eliminarDetalle((DetallePrestamo)tvDetalles.getSelectionModel().getSelectedItem());
					mngHerramienta.actualizarHerramientas();
					mngPrestamo.actualizarOutEntregate();
				}catch(NullPointerException e){
					//e.printStackTrace();
				}
			}else if(event.getSource().equals(btAgregarc)){
				String[] modulos = mngEmpleado.getEmpleadoConectado().getModulos().split(" ");
				for(String modulo:modulos){
					if(modulo.equalsIgnoreCase("crud")){
						Crud crud = new Crud(mngEmpleado, mngCliente, mngPrestamo, mngHerramienta);
						crud.agregarClienteCrud();
					}
				}
			}else if(event.getSource().equals(btAgregarp)){
				if(mngCliente.buscarCarne(tfIdCliente.getText())){
					this.eventoAgregar();
					this.agregarDetalle();
				}
			}
		}
	}
	public void agregarDetalle(){
		stgAgregard=new Stage();
		smbHerramienta = new SplitMenuButton();
		smbHerramienta.setPrefWidth(150);
		listHerramientas = mngHerramienta.obtenerHerramientas();
		smbHerramienta.setText("Elija una Herramienta");
		for(Herramienta herramienta:listHerramientas){
			smbHerramienta.getItems().add(new MenuItem(herramienta.getNombre()));
		}
		for(MenuItem item:smbHerramienta.getItems()){
			item.setOnAction(new EventHandler<ActionEvent>() {
				@Override public void handle(ActionEvent e) {
			       	smbHerramienta.setText(item.getText());
				}
			});
		}
	
		cantidadd = new TextField();
		cantidadd.setPromptText("Cantidad");
		cantidadd.addEventHandler(KeyEvent.KEY_PRESSED, this);
		gpAgregard = new GridPane();
		gpAgregard.add(smbHerramienta, 0, 0);
		gpAgregard.add(cantidadd, 0, 1);

		Scene scnAgregard = new Scene(gpAgregard);
		stgAgregard.setTitle("Agregar Herramienta");
		stgAgregard.setScene(scnAgregard);
		stgAgregard.initModality(Modality.APPLICATION_MODAL);
		stgAgregard.show();
	}
	/**
		* Metodo para obtener los detalles de un prestamo
		* @param idPrestamoh Int con el id del prestamos del que se obtendran los detalles
		* @return Retorna un TableView con los detalles del prestamo
	*/
	public TableView getDetalle(int idPrestamoh){
		tvDetalles = new TableView<DetallePrestamo>(mngPrestamo.obtenerDetalles(idPrestamoh));

		TableColumn<DetallePrestamo, String> columnaNombreHerramienta = new TableColumn<DetallePrestamo, String>("Herramienta");
		columnaNombreHerramienta.setCellValueFactory(new PropertyValueFactory<DetallePrestamo, String>("nombreHerramienta"));
		columnaNombreHerramienta.setMinWidth(120);
		TableColumn<DetallePrestamo, Integer> columnaCantidadh = new TableColumn<DetallePrestamo, Integer>("Cantidad");
		columnaCantidadh.setCellValueFactory(new PropertyValueFactory<DetallePrestamo, Integer>("cantidad"));
		columnaCantidadh.setMinWidth(120);
		tvDetalles.getColumns().addAll(columnaNombreHerramienta, columnaCantidadh);

		return tvDetalles;
	}
	/**
		* Metodo que obtiene los prestamos que se han hecho
		* @return Retorna una TableView con los prestamos
	*/
	public TableView getPrestamos(){
		this.mngPrestamo.actualizarOutEntregate();
		this.mngPrestamo.actualizarPrestamos();
		tvPrestamos = new TableView<Prestamo>(mngPrestamo.obtenerOutEntregate());
		tvPrestamos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tvPrestamos.addEventHandler(MouseEvent.MOUSE_CLICKED, this);

		TableColumn<Prestamo, Integer> columnaIdPrestamo=new TableColumn<Prestamo, Integer>("Id Prestamo");
		columnaIdPrestamo.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("idPrestamo"));
		columnaIdPrestamo.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreCliente=new TableColumn<Prestamo, String>("Cliente");
		columnaNombreCliente.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreCliente"));
		columnaNombreCliente.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreEmpleado=new TableColumn<Prestamo, String>("Empleado");
		columnaNombreEmpleado.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreEmpleado"));
		columnaNombreEmpleado.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFecha=new TableColumn<Prestamo, String>("Fecha");
		columnaFecha.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fecha"));
		columnaFecha.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFechaEntrega=new TableColumn<Prestamo, String>("Fecha de Entrega");
		columnaFechaEntrega.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fechaEntrega"));
		columnaFechaEntrega.setMinWidth(120);
		TableColumn<Prestamo, Integer> columnaCantidad=new TableColumn<Prestamo, Integer>("Cantidad");
		columnaCantidad.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("cantidad"));
		columnaCantidad.setMinWidth(120);
		TableColumn<Prestamo, String> columnaEstado=new TableColumn<Prestamo, String>("Estado");
		columnaEstado.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("estado"));
		columnaEstado.setMinWidth(120);

		tvPrestamos.getColumns().setAll(columnaIdPrestamo, columnaNombreCliente, columnaNombreEmpleado, columnaFecha, columnaFechaEntrega, columnaCantidad, columnaEstado);

		return tvPrestamos;
	}
	/**
		* Metodo para obtener los TextField vacios de un GridPane
		* @param gp GridPane del que se obtendran los TextField
		* @return Retorna un TextField vacio
	*/
	public TextField obtenerVacio(GridPane gp){
		for(Node nodo:gp.getChildren()){
			try{
				TextField txt=(TextField)nodo;
				if(txt.getText().trim().equals("")){
					return txt;
				}
			}catch(Exception e){}
		}
		return null;
	}
	/**
		* Metodo que muestra la clase
	*/
	public void mostrar(){
		this.iniciar();
	}
	/**
		* Constructor de la clase con todos los parametros
		* @param me ManejadorEmpleado a inyectar en la clase
		* @param mh ManejadorHerramienta a inyectar en la clase
		* @param mc ManejadorCliente a inyectar en la clase
		* @param mp ManejadorPrestamo a inyectar en la clase
	*/
	public Prestamista(ManejadorCliente mc, ManejadorEmpleado me, ManejadorHerramienta mh, ManejadorPrestamo mp){
		this.mngEmpleado=me;
		this.mngHerramienta=mh;
		this.mngCliente=mc;
		this.mngPrestamo=mp;
	}
	/**
		* Constructor  vacio de la clase
	*/
	public Prestamista(){
	}
}
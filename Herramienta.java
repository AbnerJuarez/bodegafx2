package org.abnerjuarez.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
	* Clase de tipo beans con las propiedades de una herramienta de la vida real
	* @author Abner A. Juarez D.
*/
public class Herramienta{
	//Propiedades
	private StringProperty nombre, categoria;
	private IntegerProperty cantidad, idHerramienta;
	/**
		* Metodo que instancia las propiedades de la clase
	*/
	public void instanciar(){
		nombre = new SimpleStringProperty();
		categoria = new SimpleStringProperty();
		cantidad = new SimpleIntegerProperty();
		idHerramienta = new SimpleIntegerProperty();
	}
	/**
		* Constructor vacio de la clase Herramienta
	*/
	public Herramienta(){
		this.instanciar();
	}
	/**
		* Constructor de la clase Herramienta con todos los parametros
		* @param idHerramienta Int con el valor del id de la herramienta
		* @param nombre String con el valor del nombre de la herramienta
		* @param categoria String con el valor de la categoria de la herramienta
		* @param cantidad Int con el valor de la cantidad en stand de la herramienta
	*/
	public Herramienta(int idHerramienta, String nombre, String categoria, int cantidad){
		this.instanciar();
		this.setIdHerramienta(idHerramienta);
		this.setNombre(nombre);
		this.setCategoria(categoria);
		this.setCantidad(cantidad);
	}
	/**
		* Constructor de la clase Herramienta con todos los parametros excepto el idHerramienta
		* @param nombre String con el valor del nombre de la herramienta
		* @param categoria String con el valor de la categoria de la herramienta
		* @param cantidad Int con el valor de la cantidad en stand de la herramienta
	*/
	public Herramienta(String nombre, String categoria, int cantidad){
		this.instanciar();
		this.setNombre(nombre);
		this.setCategoria(categoria);
		this.setCantidad(cantidad);
	}
	/**
		* Settea el valor del IdHerramienta
		* @param id Int con el valor del id de la herramienta
	*/
	public void setIdHerramienta(int id){
		this.idHerramienta.set(id);
	}
	/**
		* Settea el valor del nombre de la herramienta
		* @param nombre String con el valor del nombre de la herramienta
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
		* Settea el valor de la categoria de la herramienta
		* @param categoria String con el valor de la categoria de la herramienta
	*/
	public void setCategoria(String categoria){
		this.categoria.set(categoria);
	}
	/**
		* Settea el valor de la cantidad en stand de la herramienta
		* @param cantidad Int con el valor de la cantidad en stand de la herramienta
	*/
	public void setCantidad(int cantidad){
		this.cantidad.set(cantidad);
	}
	/**
		* Obtiene el valor del idHerramienta
		* @return Retorna un int con el valor del idHerramienta
	*/
	public int getIdHerramienta(){
		return this.idHerramienta.get();
	}
	/**
		* Obtiene el valor del nombre de la herramienta
		* @return Retorna un String con el valor del nombre de la herramienta
	*/
	public String getNombre(){
		return this.nombre.get();
	}
	/**
		* Obtiene el valor de la categoria de la herramienta
		* @return Retorna un String con el valor de la categoria de la herramienta
	*/
	public String getCategoria(){
		return this.categoria.get();
	}
	/**
		* Obtiene el valor de la cantidad en stand de la herramienta
		* @return Retorna un int con el valor de la cantidad en stand de la herramienta
	*/
	public int getCantidad(){
		return this.cantidad.get();
	}
}
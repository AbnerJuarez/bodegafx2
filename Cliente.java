package org.abnerjuarez.beans;

import org.abnerjuarez.beans.Usuario;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
/**
	* Clase tipo beans con todas las propiedades que debe llevar un Cliente
	* @author Abner A. Juarez D.
*/
public class Cliente extends Usuario{
	StringProperty carne, seccion, jornada;
	/**
		* Constructor vacio de la clase Cliente
	*/
	public Cliente(){
		super.instanciar();
		this.instanciar();
	}
	/**
		* Constructor con todos los parametros de la clase Cliente
		* @param idCliente Int con el valor del idCliente
		* @param nombre String con el valor del nombre
		* @param apellido String con el valor del apellido
		* @param carne String con el valor del carne
		* @param seccion String con el valor de la seccion
		* @param jornada String con el valor de la jornada
	*/
	public Cliente(int idCliente, String nombre, String apellido, String carne, String seccion, String jornada){
		super.instanciar();
		this.instanciar();
		super.setIdUsuario(idCliente);
		super.setNombre(nombre);
		super.setApellido(apellido);
		this.setCarne(carne);
		this.setSeccion(seccion);
		this.setJornada(jornada);
	}
	/**
		* Metodo que instancia las propiedades del usuario
	*/
	public void instanciar(){
		this.carne= new SimpleStringProperty();
		this.seccion = new SimpleStringProperty();
		this.jornada = new SimpleStringProperty();
	}
	/**
		* Setta el valor del carne del usuario
		* @param carne String con el valor del carne
	*/
	public void setCarne(String carne){
		this.carne.set(carne);
	}
	/**
		* Obtiene el carne del usuario
		* @return Retorna un String con el valor del carne
	*/
	public String getCarne(){
		return this.carne.get();
	}
	/**
		* Settea el valor de la seccion del usuario
		* @param seccion Recibe un String con el valor de la seccion
	*/
	public void setSeccion(String seccion){
		this.seccion.set(seccion);
	}
	/**
		* Obtiene la seccion del usuario
		* @return Retorna un String con el valor de la seccion
	*/
	public String getSeccion(){
		return this.seccion.get();
	}
	/**
		* Settea el valor de la jornada del usuario
		* @param jornada String con el valor de la jornada
	*/
	public void setJornada(String jornada){
		this.jornada.set(jornada);
	}
	/**
		* Obtiene el valor de la jornada del usuario
		* @return Retorna un String con el valor de la jornada
	*/
	public String getJornada(){
		return this.jornada.get();
	}
}
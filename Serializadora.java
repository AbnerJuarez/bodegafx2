package org.abnerjuarez.recursos;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Serializadora{
	private ObjectInputStream lector;
	private ObjectOutputStream escritor;
	public void serializar(Object object){
		try{
			this.escritor = new ObjectOutputStream(new FileOutputStream("usuarios.dat"));
			this.escritor.writeObject(object);
		}catch(FileNotFoundException e){
			System.out.println("File Not Found");
			e.printStackTrace();
		}catch(IOException e){
			System.out.println("In/Out Exception");
			e.printStackTrace();
		}
	}
	public Object deserializar(String archivo){
		Object retorno = null;
		try{
			this.lector = new ObjectInputStream(new FileInputStream(archivo));
			retorno = lector.readObject();
		}catch(FileNotFoundException e){
			System.out.println("File Not Found");
			e.printStackTrace();
		}catch(IOException e){
			System.out.println("In/Out Exception");
			e.printStackTrace();
		}catch(ClassNotFoundException e){
			System.out.println("Class Not Found");
			e.printStackTrace();
		}
		return retorno;
	}
}
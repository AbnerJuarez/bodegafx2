package org.abnerjuarez.visual;

import org.abnerjuarez.manejadores.ManejadorEmpleado;
import org.abnerjuarez.beans.Empleado;
import org.abnerjuarez.visual.Modulos;
import org.abnerjuarez.db.Conection;
import org.abnerjuarez.recursos.Serializadora;

import javafx.scene.control.Button;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.geometry.Side;
import java.util.ArrayList;

/**
	* Clase encargada de la autenticación de usuarios
	* @author Abner A. Juarez D.
*/

public class App extends Application implements EventHandler<Event>{
	//Propiedades
	private Button btLogin;
	private Label lblPassword, lblUser;
	private TextField tfUser;
	private PasswordField pfPassword;
	private GridPane gpLogin;
	private Scene sceneLogin;
	private Stage segunda;
	private ManejadorEmpleado mngEmpleado;
	private Modulos modulos;
	private String[] moduloss;
	private Conection conect;
	private ContextMenu sugerencias;
	private ArrayList<String> usuarios;
	private Serializadora serializadora;
    private static final String CSS_TEXT_FIELD_INVALID = "text-field-invalid";
    /**
		* Constructor vacio para la clase App
    */
	public App(){}
	/**
		* Metodo start heredado de Application
		* @param primary Stage que carga la ventana primaria
	*/
	public void start(Stage primary){
		this.iniciar();
	}
	/**
		* Metodo encargada de inicializar las propiedades y ventanas de la clase App
	*/
	public void iniciar(){
		this.serializadora = new Serializadora();
		this.usuarios = new ArrayList();
		try{
			this.usuarios.addAll((ArrayList<String>)serializadora.deserializar("usuarios.dat"));
		}catch(NullPointerException e){
			System.out.println("Archivo sin datos");
		}
		this.sugerencias = new ContextMenu();
		this.conect = new Conection();
		this.mngEmpleado = new ManejadorEmpleado(conect);
		this.segunda = new Stage();
		this.btLogin = new Button("Log In");
		this.btLogin.addEventHandler(ActionEvent.ACTION, this);
		this.lblPassword = new Label("Password");
		this.lblUser = new Label("Usuario");
		this.tfUser = new TextField();
		this.itemizar(tfUser.getText());
		this.tfUser.setContextMenu(sugerencias);
		this.tfUser.setPromptText("Usuario");
		this.pfPassword = new PasswordField();
		this.pfPassword.setPromptText("Password");
		this.pfPassword.addEventHandler(KeyEvent.KEY_PRESSED, this);
		this.tfUser.addEventHandler(KeyEvent.KEY_PRESSED, this);
		this.tfUser.addEventHandler(KeyEvent.KEY_PRESSED, this);
		this.gpLogin = new GridPane();
		this.gpLogin.add(lblUser, 0, 0);
		this.gpLogin.add(tfUser, 1, 0);
		this.gpLogin.add(lblPassword, 0, 1);
		this.gpLogin.add(pfPassword, 1, 1);
		this.gpLogin.add(btLogin, 1, 2);
		this.gpLogin.setVgap(5);
		Scene sceneLogin= new Scene(gpLogin);
		sceneLogin.getStylesheets().add("app.css");
		this.segunda.setScene(sceneLogin);
		this.segunda.setTitle("Login");
		this.segunda.setWidth(226);
		this.segunda.show();
	}
	/**
		* Metodo encargado de manejar los eventos de la clase
	*/
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource().equals(btLogin)){
				this.eventoLogin();
			}
		}else if(event instanceof KeyEvent){
			KeyEvent kevent = (KeyEvent)event;
			if(kevent.getCode() == KeyCode.ENTER){
				sugerencias.hide();
				if(event.getSource().equals(tfUser) || event.getSource().equals(pfPassword)){
					this.eventoLogin();
				}
			}else if(kevent.getEventType() == KeyEvent.KEY_PRESSED){
				if(!tfUser.getText().trim().equals("")){
					if(event.getSource().equals(tfUser)){
						this.itemizar(tfUser.getText());
						sugerencias.show(tfUser, Side.BOTTOM, 0, 0);
						tfUser.requestFocus();
					}
				}else{
					sugerencias.hide();
				}
			}
		}
	}
	/**
		* Metodo activado por los eventos del Login
	*/
	public void eventoLogin(){
		if(this.obtenerVacio(gpLogin)!=null){
			TextField vacio=this.obtenerVacio(gpLogin);
			vacio.requestFocus();
		}else if(mngEmpleado.login(tfUser.getText(), pfPassword.getText())){
			if(usuarios.contains(tfUser.getText())==false){
				this.usuarios.add(tfUser.getText());
			}
			Empleado conectado=mngEmpleado.getEmpleadoConectado();
			this.moduloss=conectado.getModulos().split(" ");
			this.modulos = new Modulos(moduloss, mngEmpleado);
			this.modulos.iniciar();
			this.serializadora.serializar(usuarios);
			segunda.close();
		}else if(mngEmpleado.login(tfUser.getText(), pfPassword.getText())==false){
			if(mngEmpleado.buscarId(tfUser.getText())==true){
				pfPassword.getStyleClass().add(CSS_TEXT_FIELD_INVALID);
				tfUser.getStyleClass().remove(CSS_TEXT_FIELD_INVALID);
				pfPassword.setText("");
			}else{
				tfUser.getStyleClass().add(CSS_TEXT_FIELD_INVALID);
				pfPassword.getStyleClass().add(CSS_TEXT_FIELD_INVALID);
				pfPassword.setText("");
			}
		}
	}
	/**
		* Metodo encargado de volver items los id de usuario para el autocompletar
		* @param id String con el id a autocompletar
	*/
	public void itemizar(String id){
		this.sugerencias.getItems().clear();
		for(String usuario:usuarios){
			if(id.trim().equals(""))
				sugerencias.getItems().add(new MenuItem(usuario));
			else{
				if(usuario.contains(id)==true){
					if(!usuario.equals("aron"))
						sugerencias.getItems().add(new MenuItem(usuario));
				}
			}
		}
		for(MenuItem item:sugerencias.getItems()){
			item.setOnAction(new EventHandler<ActionEvent>(){
				@Override public void handle(ActionEvent e) {
					tfUser.setText(item.getText());
					tfUser.nextWord();
					tfUser.requestFocus();
					sugerencias.hide();
				}
			});
		}
	}
	/**
		* Metodo que retorna los textfield vacios de un gridpane
		* @param gp GridPane para extraer los textfield vacio
		* @return Retorna un TextField vacio
	*/
	public TextField obtenerVacio(GridPane gp){
		for(Node nodo:gp.getChildren()){
			try{
				TextField txt=(TextField)nodo;
				if(txt.getText().trim().equals("")){
					return txt;
				}
			}catch(Exception e){}
		}
		return null;
	}
	/**
		* Metodo para verificar los datos de las credenciales
		* @param user String con la credencial de identificacion
		* @param pass String con la credencial de contraseña
		* @return Retorna un boolean respectivo a si estan llenos o no
	*/
	public boolean verificarDatos(String user, String pass){
		if(user.trim().equals("") && pass.trim().equals(""))
			return true;
		return false;
	}
}
package org.abnerjuarez.manejadores;

import org.abnerjuarez.beans.Herramienta;
import org.abnerjuarez.db.Conection;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
	* Clase de tipo manejador que administra el beans Herramienta
	* @author Abner A. Juarez D.
*/
public class ManejadorHerramienta{
	//Propiedades
	private ObservableList<Herramienta> herramientas;
	private Conection conect;
	/**
		* Constructor de la clase ManejadorHerramienta
		* @param conect Objeto tipo Conection con la conexion
	*/
	public ManejadorHerramienta(Conection conect){
		this.conect=conect;
		this.herramientas=FXCollections.observableArrayList();
	}
	/**
		* Metodo para actualizar las herramientas
	*/
	public void actualizarHerramientas(){
		ResultSet result = conect.ejecutarConsulta("SELECT idHerramienta, nombre, categoria, cantidad FROM Herramienta");
		if(result!=null){
			herramientas.clear();
			try{
				while(result.next()){
					Herramienta herramienta = new Herramienta(result.getInt("idHerramienta"), result.getString("nombre"), result.getString("categoria"), result.getInt("cantidad"));
					herramientas.add(herramienta);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
	/**
		* Metodo para obtener la lista de herramientas
		* @return Retorna una lista ObservableList con las herramientas
	*/
	public ObservableList<Herramienta> obtenerHerramientas(){
		this.actualizarHerramientas();
		return this.herramientas;
	}
	/**
		* Metodo para insertar herramientas
		* @param nombre String con el nombre de la herramienta
		* @param categoria String con la categoria de la herramienta
		* @param cantidad Int con la cantidad de la herramienta
	*/
	public void insertarHerramienta(String nombre, String categoria, int cantidad){
		if(this.buscarNombre(nombre)==false){
			conect.ejecutarSentencia("INSERT INTO Herramienta (nombre, categoria, cantidad) VALUES ('"+nombre+"', '"+categoria+"', "+cantidad+")");
			actualizarHerramientas();
		}
	}
	/**
		* Metodo para eliminar una herramienta
		* @param herramienta Recibe un objeto Herramienta con los valores de la eliminacion
	*/
	public void eliminarHerramienta(Herramienta herramienta){
		conect.ejecutarSentencia("DELETE FROM Herramienta WHERE idHerramienta="+herramienta.getIdHerramienta());
		actualizarHerramientas();
	}
	/**
		* Metodo para modifiar una herramienta
		* @param herramienta Recibe una herramienta para modificarla
	*/
	public void modificarHerramienta(Herramienta herramienta){
		conect.ejecutarSentencia("UPDATE Herramienta SET nombre='"+herramienta.getNombre()+"', categoria='"+herramienta.getCategoria()+"', cantidad='"+herramienta.getCantidad()+"' WHERE idHerramienta="+herramienta.getIdHerramienta());
		this.actualizarHerramientas();
	}
	/**
		* Busca si ya existe una herramienta con la misma marca
		* @param nombre Recibe un String con el nombre a evaluar
		* @return Retorna un boolean respectivo a si lo encontro o no
	*/
	public boolean buscarNombre(String nombre){
		ResultSet result = conect.ejecutarConsulta("SELECT nombre FROM Herramienta WHERE nombre='"+nombre+"'");
		try{
			while(result.next()){
				return true;
			}
		}catch(SQLException sql){}
		return false;
	}
	/**
		* Metodo para buscar una herramienta en la list de herramientas segun su marca
		* @param nombre String con el nombre de la herramienta a buscar
		* @return Retorna un objeto tipo Herramienta con la herramienta encontrada
	*/
	public Herramienta buscarHerramienta(String nombre){
		ResultSet result = conect.ejecutarConsulta("SELECT * FROM Herramienta WHERE nombre='"+nombre+"'");
		try{
			while(result.next()){
				Herramienta herramienta = new Herramienta(result.getInt("idHerramienta"), result.getString("nombre"), result.getString("categoria"), result.getInt("cantidad"));
				return herramienta;
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
		return null;
	}
	/**
		* Metodo para buscar una herramienta segun su nombre
		* @param idHerramienta Int con el id de la herramienta a buscar
		* @return Retorna un objeto tipo Herramienta con la herramienta encontrada
	*/
	public Herramienta buscarHerramienta(int idHerramienta){
		this.actualizarHerramientas();
		for(Herramienta herra:herramientas){
			if(herra.getIdHerramienta()==idHerramienta)
				return herra;
		}
		return null;
	}
	/**
		* Metodo para buscar herramientas segun un parametro
		* @param parametro String con el parametro de busqueda
		* @return Retorna un ArrayList con las herramientas encontradas segun el parametro
	*/
	public ArrayList<Herramienta> buscarHerramientaAvanzada(String parametro){
		this.actualizarHerramientas();
		ArrayList<Herramienta> encontrados = new ArrayList<Herramienta>();
		for(Herramienta herramienta:herramientas){
			parametro = parametro.toLowerCase();
			if(herramienta.getNombre().toLowerCase().contains(parametro) || herramienta.getCategoria().toLowerCase().contains(parametro)){
				encontrados.add(herramienta);
			}
		}
		return encontrados;
	}
}
package org.abnerjuarez.beans;

import org.abnerjuarez.beans.Usuario;

/**
	* Clase de tipo beans con las propiedades para simular un empleado, extiende de usuario
	* @author Abner A. Juarez D.
*/
public class Empleado extends Usuario{
	/**
		* Constructor vacio de la clase Empleado
	*/
	public Empleado(){
		super.instanciar();
	}
	/**
		* Constructor con todos los parametros de la clase Empleado
		* @param idEmpleado Int con el valor del idEmpleado
		* @param nombre String con el valor del nombre del empleado
		* @param apellido String con el valor del apellido del empleado
		* @param modulos String con el valor de los modulso del empleado
		* @param password String con el valor del password que usara el empleado en el loggeo
		* @param id String con el valor del id que usara el usuario en el loggeo
	*/
	public Empleado(int idEmpleado, String nombre, String apellido, String modulos, String password, String id){
		super.instanciar();
		super.setIdUsuario(idEmpleado);
		super.setNombre(nombre);
		super.setApellido(apellido);
		super.setModulos(modulos);
		super.setPassword(password);
		super.setId(id);
	}
}
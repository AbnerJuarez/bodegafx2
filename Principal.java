package org.abnerjuarez.sistema;

import javafx.application.Application;
import org.abnerjuarez.visual.Crud;
import org.abnerjuarez.visual.MessageDialog;
import org.abnerjuarez.visual.PruebaFlechas;
import org.abnerjuarez.visual.Prestamista;
import org.abnerjuarez.visual.App;

/**
	* Esta es la clase principal encargada de inicializar la Aplicacion
	* @author Abner Ariel Juarez Delgado
*/
public class Principal{
	/**
		* Metodo principal main
		* @param args Array de String con los argumentos de inicio
	*/
	public static void main(String[] args){
		Application.launch(App.class, args);
	}
}
package org.abnerjuarez.recursos;

import org.abnerjuarez.recursos.Eliminador;
import org.abnerjuarez.beans.Herramienta;
import org.abnerjuarez.beans.Cliente;
import org.abnerjuarez.beans.Empleado;
import org.abnerjuarez.manejadores.ManejadorEmpleado;
import org.abnerjuarez.manejadores.ManejadorCliente;
import org.abnerjuarez.manejadores.ManejadorHerramienta;

import javafx.event.EventHandler;
import javafx.scene.input.*;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;

/**
        * Clase que administra el Drag y el Drop del programa
        * @author Abner A. Juarez D.
*/
public class DragDrop{
                //Propiedades
                private String tipo;
                private Eliminador eliminador;

                /**
                                * Metodo para settear el tipo de DragAndDrop
                                * @param tipo String con el valor del tipo
                */
                public void setTipo(String tipo){
                    this.tipo=tipo;
                }
                /**
                                * Metodo para inyectar el eliminador de la clase
                                * @param e Eliminador para ser inyectado en la clase
                */
                public void setEliminador(Eliminador e){
                    this.eliminador=e;
                }
                /**
                                * Constructor vacio de la clase
                */
                public DragDrop(){}
                /**
                                * Metodo que prepara el objetivo para un DragAndDrop
                                * @param target Node que se preparara para el DragAndDrop
                */
                public void setTarget(Node target){
                    //Prepara el Drag en el objetivo para aceptar la transferencia
                    target.setOnDragOver(new EventHandler<DragEvent>(){
                        public void handle(DragEvent event) {
                            if(event.getGestureSource()!=target && event.getDragboard().hasString()){
                                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                            }
                            event.consume();
                        }
                    });
                    //Cuando el Drag esta encima del objetivo
                    target.setOnDragEntered(new EventHandler<DragEvent>(){
                        public void handle(DragEvent event){
                            if(event.getGestureSource() != target && event.getDragboard().hasString()){
                                if(target instanceof Button)
                                    ((Button)target).setText("DROP HERE");
                            }
                            event.consume();
                        }
                    });
                    //Cuando el Drag sale del objetivo
                    target.setOnDragExited(new EventHandler<DragEvent>(){
                        public void handle(DragEvent event){
                            if(target instanceof Button)
                                ((Button)target).setText("Eliminar");
                            event.consume();
                        }
                    });
                //Cuando se Droppea el objetivo con la transferencia
                    target.setOnDragDropped(new EventHandler<DragEvent>(){
                        public void handle(DragEvent event){
                            Dragboard db = event.getDragboard();
                            boolean success = false;
                            if(db.hasString()){
                                if(tipo.equals("Cliente"))
                                    eliminador.eliminarCliente(Integer.parseInt(db.getString()));
                                else if(tipo.equals("Empleado"))
                                    eliminador.eliminarEmpleado(Integer.parseInt(db.getString()));
                                else if(tipo.equals("Herramienta"))
                                    eliminador.eliminarHerramienta(Integer.parseInt(db.getString()));
                                success = true;
                            }
                            event.setDropCompleted(success);
                            event.consume();
                        }
                    });
                }
}
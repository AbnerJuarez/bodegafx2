package org.abnerjuarez.visual;

import org.abnerjuarez.manejadores.ManejadorPrestamo;
import org.abnerjuarez.manejadores.ManejadorHerramienta;
import org.abnerjuarez.manejadores.ManejadorCliente;
import org.abnerjuarez.manejadores.ManejadorEmpleado;
import org.abnerjuarez.visual.Prestamista;
import org.abnerjuarez.visual.Crud;
import org.abnerjuarez.visual.Visor;
import org.abnerjuarez.db.Conection;

import javafx.application.Application;
import javafx.event.Event;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.scene.Node;
import javafx.event.ActionEvent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.stage.StageStyle;
import javafx.scene.control.ToolBar;
import javafx.geometry.Side;

/**
	* Clase que carga los modulos segun el usuario que ha sido loggeado
	* @author Abner A. Juarez D.
*/
public class Modulos extends Application implements EventHandler<Event>{
	//Propiedades
	private Stage stgPrincipal;
	private Scene scnPrincipal;
	private Button btPrestamista, btCrud, btVisor, btLogout;
	private String[] modulos;
	private ToolBar tbModulos;
	private ManejadorEmpleado mngEmpleado;
	private ManejadorCliente mngCliente;
	private ManejadorHerramienta mngHerramienta;
	private ManejadorPrestamo mngPrestamo;
	private Visor visor;
	private Prestamista prestamista;
	private Conection conect;
	private Crud crud;
	private ContextMenu funcion;
	private App login;
	private MenuItem item;
	private ImageView imgViewCrud, imgViewPresta, imgViewVisor, imgViewLogout;

	/**
		* Constructor de la clase con 2 parametros
		* @param modulos Array de String con los modulos del usuario
		* @param mngEmpleado ManejadorEmpleado que se inyectara en la clase
	*/
	public Modulos(String[] modulos, ManejadorEmpleado mngEmpleado){
		this.stgPrincipal = new Stage(StageStyle.TRANSPARENT);
		this.stgPrincipal.getIcons().add(new Image("utility.png"));
		this.mngEmpleado = mngEmpleado;
		this.modulos = modulos;
		tbModulos = new ToolBar();
	}
	/**
		* Metodo de inicio heredado de Application
		* @param primary Stage con la ventana primaria de la aplicacion
	*/
	public void start(Stage primary){
	}
	/**
		* Metodo que inicia la clase
	*/
	public void iniciar(){
		this.funcion = new ContextMenu();
		this.item = new MenuItem();
		funcion.getItems().add(item);
		this.conect = new Conection();
		this.mngPrestamo = new ManejadorPrestamo(conect);
		this.mngHerramienta = new ManejadorHerramienta(conect);
		this.mngCliente = new ManejadorCliente(conect);
		for(String modulo:modulos){
			if(modulo.equals("Prestamista") || modulo.equals("prestamista")){
				imgViewPresta = new ImageView(new Image(this.getClass().getResourceAsStream("presta.png")));
				imgViewPresta.setFitWidth(75);
				imgViewPresta.setFitHeight(80);
				btPrestamista = new Button("", imgViewPresta);
				btPrestamista.addEventHandler(ActionEvent.ACTION, this);
				btPrestamista.addEventHandler(MouseEvent.MOUSE_ENTERED, this);
				btPrestamista.addEventHandler(MouseEvent.MOUSE_EXITED, this);
				tbModulos.getItems().add(btPrestamista);
			}else if(modulo.equals("Visor") || modulo.equals("visor")){
				imgViewVisor = new ImageView(new Image(this.getClass().getResourceAsStream("visor.png")));
				imgViewVisor.setFitWidth(75);
				imgViewVisor.setFitHeight(80);
				btVisor = new Button("", imgViewVisor);
				btVisor.addEventHandler(ActionEvent.ACTION, this);
				btVisor.addEventHandler(MouseEvent.MOUSE_ENTERED, this);
				btVisor.addEventHandler(MouseEvent.MOUSE_EXITED, this);
				tbModulos.getItems().add(btVisor);
			}else if(modulo.equals("Crud") || modulo.equals("crud") || modulo.equals("CRUD")){
				imgViewCrud = new ImageView(new Image(this.getClass().getResourceAsStream("crud.png")));
				imgViewCrud.setFitWidth(75);
				imgViewCrud.setFitHeight(80);
				btCrud = new Button("", imgViewCrud);
				btCrud.addEventHandler(ActionEvent.ACTION, this);
				btCrud.addEventHandler(MouseEvent.MOUSE_ENTERED, this);
				btCrud.addEventHandler(MouseEvent.MOUSE_EXITED, this);
				tbModulos.getItems().add(btCrud);
			}
		}
		imgViewLogout = new ImageView(new Image(this.getClass().getResourceAsStream("logout.png")));
		imgViewLogout.setFitWidth(80);
		imgViewLogout.setFitHeight(80);
		this.btLogout = new Button("", imgViewLogout);
		this.btLogout.addEventHandler(ActionEvent.ACTION, this);
		btLogout.addEventHandler(MouseEvent.MOUSE_ENTERED, this);
		btLogout.addEventHandler(MouseEvent.MOUSE_EXITED, this);
		tbModulos.getItems().add(btLogout);

		scnPrincipal = new Scene(tbModulos);
		scnPrincipal.getStylesheets().add("JMetroDarkTheme.css");
		stgPrincipal.setScene(scnPrincipal);
		stgPrincipal.centerOnScreen();
		stgPrincipal.show();
	}
	/**
		* Metodo que maneja los eventos de la clase
		* @param event Event con el evento que se autogenero en la clase
	*/
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource().equals(btPrestamista)){
				this.prestamista = new Prestamista(mngCliente, mngEmpleado, mngHerramienta, mngPrestamo);
				this.prestamista.iniciar();
			}else if(event.getSource().equals(btCrud)){
				this.crud = new Crud(mngEmpleado, mngCliente, mngPrestamo, mngHerramienta);
				this.crud.iniciar();
			}else if(event.getSource().equals(btVisor)){
				this.visor = new Visor(mngPrestamo);
				this.visor.iniciar();
			}else if(event.getSource().equals(btLogout)){
				mngEmpleado.desloggearEmpleado();
				this.login = new App();
				if(prestamista!=null)
					this.prestamista.cerrar();
				if(crud!=null)
					this.crud.cerrar();
				if(visor!=null)
					this.visor.cerrar();
				this.login.iniciar();
				this.stgPrincipal.close();
			}
		}else if(event instanceof MouseEvent){
			MouseEvent mevent=(MouseEvent)event;
			if(mevent.getEventType()==MouseEvent.MOUSE_ENTERED){
				if(event.getSource().equals(btPrestamista)){
					this.item.setGraphic(new Label("Modulo Prestamista, \npuede crear y eliminar prestamos."));
					funcion.show(btPrestamista, Side.BOTTOM, 0, 0);
				}else if(event.getSource().equals(btCrud)){
					this.item.setGraphic(new Label("Modulo CRUD, \npuede crear, eliminar y modificar \nlas entidades del programa."));
					funcion.show(btCrud, Side.BOTTOM, 0, 0);
				}else if(event.getSource().equals(btVisor)){
					this.item.setGraphic(new Label("Modulo Visor, \npuede ver todos los prestamos y su estado."));
					funcion.show(btVisor, Side.BOTTOM, 0, 0);
				}else if(event.getSource().equals(btLogout)){
					this.item.setGraphic(new Label("Salir"));
					funcion.show(btLogout, Side.BOTTOM, 0, 0);
				}
			}else if(mevent.getEventType()==MouseEvent.MOUSE_EXITED){
				if(event.getSource().equals(btPrestamista) || event.getSource().equals(btCrud) || event.getSource().equals(btVisor) || event.getSource().equals(btLogout)){
					funcion.hide();
				}
			}
		}
	}
}
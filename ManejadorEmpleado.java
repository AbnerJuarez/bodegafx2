package org.abnerjuarez.manejadores;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.abnerjuarez.db.Conection;
import org.abnerjuarez.beans.Empleado;

/**
	* Clase tipo manejador encargada de administrar el beans Empleado
	* @author Abner A. Juarez D.
*/
public class ManejadorEmpleado{
	//Propiedades
	private ObservableList<Empleado> empleados;
	private Conection conect;
	private Empleado conectado;
	/**
		* Constructor de la clase
		* @param conect Conection con la conexion establecida
	*/
	public ManejadorEmpleado(Conection conect){
		this.conect=conect;
		this.empleados=FXCollections.observableArrayList();
	}
	/**
		* Settea el valor del empleado conectado
		* @param empleado Objeto tipo Empleado con el valor a insertar
	*/
	public void setEmpleadoConectado(Empleado empleado){
		this.conectado=empleado;
	}
	/**
		* Metodo para deslogger al empleado conectado
	*/
	public void desloggearEmpleado(){
		this.conectado=null;
	}
	/**
		* Obtiene el valor del empleado conectado
		* @return Retorna el valor del Empleado conectado
	*/
	public Empleado getEmpleadoConectado(){
		return conectado;
	}
	/**
		* Metodo para validar el loggeo del empleado
		* @param id String con el id del empleado
		* @param password String con el password del empleado
		* @return Retorna una boolean respectivo a si se loggeo o no
	*/
	public boolean login(String id, String password){
		if(conectado!=null)
			return true;
		ResultSet resultado = conect.ejecutarConsulta("SELECT idEmpleado, nombre, apellido, modulos, pass, id FROM Empleado WHERE id='"+id+"' AND pass='"+password+"'");
		try{
			if(resultado!= null){
				if(resultado.next())
					conectado = new Empleado(resultado.getInt("idEmpleado"), resultado.getString("nombre"), resultado.getString("apellido"), resultado.getString("modulos"), resultado.getString("pass"), resultado.getString("id"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return conectado!=null;
	}
	/**
		* Metodo para actualizar la lista de los empleados en la base de datos
	*/
	public void actualizarEmpleados(){
		ResultSet result = conect.ejecutarConsulta("SELECT idEmpleado, nombre, apellido, modulos, pass, id FROM Empleado");
		if(result!=null){
			empleados.clear();
			try{
				while(result.next()){
					Empleado empleado = new Empleado(result.getInt("idEmpleado"), result.getString("nombre"), result.getString("apellido"), result.getString("modulos"), result.getString("pass"), result.getString("id"));
					empleados.add(empleado);
				}
			}catch(SQLException e){}
		}
	}
	/**
		* Obtiene el valor de la lista de los empleados en la base de datos
		* @return Retorna un ObservableList con el valor de los empleados
	*/
	public ObservableList<Empleado> obtenerEmpleados(){
		this.actualizarEmpleados();
		return this.empleados;
	}
	/**
		* Metodo para agregar empleados en la base de datos
		* @param nombre String con el valor del nombre del empleado
		* @param apellido String con el valor del apellido del empleado
		* @param modulos String con el valor de los modulos que podra tener el empleado
		* @param pass String con el valor del password que tendra el empleado
		* @param id String con el valor del id que usara el empleado
	*/
	public void agregarEmpleado(String nombre, String apellido, String modulos, String pass, String id){
		if(this.buscarId(id)==false){
			conect.ejecutarSentencia("INSERT INTO Empleado(nombre, apellido, modulos, pass, id) VALUES ('"+nombre+"', '"+apellido+"', '"+modulos+"', '"+pass+"', '"+id+"')");
			this.actualizarEmpleados();
		}
	}
	/**
		* Metodo para eliminar un empleado de la base de datos
		* @param empleado Objeto tipo empleado con el valor del empleado a eliminar
	*/
	public void eliminarEmpleado(Empleado empleado){
		conect.ejecutarSentencia("DELETE FROM Empleado WHERE idEmpleado="+empleado.getIdUsuario());
		actualizarEmpleados();
	}
	/**
		* Metodo para modificar un empleado de la base de datos
		* @param empleado Objeto tipo Empleado con el valor del empleado a modificar
	*/
	public void modificarEmpleado(Empleado empleado){
		conect.ejecutarSentencia("UPDATE Empleado SET nombre='"+empleado.getNombre()+"', apellido='"+empleado.getApellido()+"', modulos='"+empleado.getModulos()+"', pass='"+empleado.getPassword()+"', id='"+empleado.getId()+"' WHERE idEmpleado="+empleado.getIdUsuario());
		actualizarEmpleados();
	}
	/**
		* Metodo que comprueba si ya existe un id en la base de datos
		* @param id String con el valor del id a buscar
		* @return Retorna un boolean con respectivo a si existe o no el id
	*/
	public boolean buscarId(String id){
		ResultSet result=conect.ejecutarConsulta("SELECT idEmpleado FROM Empleado WHERE id='"+id+"'");
		try{
			while(result.next()){
				return true;
			}
		}catch(SQLException sql){}
		return false;
	}
	/**
		* Metodo para buscar un empleado en la lista de empleados
		* @param idEmpleado Int con el id del empleado a buscar
		* @return Retorna un objeto Empleado con el empleado encontrado
	*/
	public Empleado buscarEmpleado(int idEmpleado){
		this.actualizarEmpleados();
		for(Empleado empleado:empleados){
			if(empleado.getIdUsuario()==idEmpleado)
				return empleado;
		}
		return null;
	}
	/**
		* Metodo para buscar empleados segun un parametro de busqueda
		* @param parametro String con el parametro de busqueda
		* @return Retorna un ArrayList de los empleados encontrados
	*/
	public ArrayList<Empleado> buscarEmpleadoAvanzada(String parametro){
		this.actualizarEmpleados();
		ArrayList<Empleado> encontrados = new ArrayList<Empleado>();
		for(Empleado empleado:empleados){
			parametro = parametro.toLowerCase();
			if(empleado.getNombre().toLowerCase().contains(parametro) || empleado.getApellido().toLowerCase().contains(parametro) || empleado.getId().toLowerCase().contains(parametro)){
				encontrados.add(empleado);
			}
		}
		return encontrados;
	}
}
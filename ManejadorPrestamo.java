package org.abnerjuarez.manejadores;

import org.abnerjuarez.beans.Prestamo;
import org.abnerjuarez.beans.DetallePrestamo;
import org.abnerjuarez.db.Conection;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
	* Clase tipo Manejador que administra el beans Prestamo
	* @author Abner A. Juarez D.
*/
public class ManejadorPrestamo{
	//Propiedades
	private ObservableList<Prestamo> prestamos, outEntregate, entregados, fueraFecha, pendientes;
	private ObservableList<DetallePrestamo> detalles;
	private Conection conect;
	/**
		* Constructor de la clase ManejadorPrestamo
		* @param conect Recibe un objeto de tipo Conection
	*/
	public ManejadorPrestamo(Conection conect){
		this.conect=conect;
		this.prestamos=FXCollections.observableArrayList();
		this.detalles=FXCollections.observableArrayList();
		//OutEntregate para modulo Prestamista, excluye los que ya han sido entregados
		this.outEntregate=FXCollections.observableArrayList();
		this.entregados=FXCollections.observableArrayList();
		this.fueraFecha=FXCollections.observableArrayList();
		this.pendientes=FXCollections.observableArrayList();
	}
	/**
		* Metodo para insertar un Prestamo
		* @param idCliente Recibe un int con el idCliente
		* @param idEmpleado Recibe un int con el idEmpleado
		* @param fechaEntrega Recibe un String con la fecha de Entrega
		* @param cantidad Recibe un int con la cantidad del prestamo
		* @param estado Recibe un String con el estado del prestamo
	*/
	public void insertarPrestamo(int idCliente, int idEmpleado, String fechaEntrega, int cantidad, String estado){
		String hoy=this.generarFecha();
		conect.ejecutarSentencia("INSERT INTO Prestamo(idCliente, idEmpleado, fecha, fechaEntrega, cantidad, estado) VALUES ('"+idCliente+"', '"+idEmpleado+"', '"+hoy+"', '"+fechaEntrega+"','"+cantidad+"', '"+estado+"')");
		this.actualizarPrestamos();
	}
	public void insertarDetalle(int idPrestamo, int idHerramienta, int cantidad){
		conect.ejecutarSentencia("INSERT INTO DetallePrestamo(idPrestamo, idHerramienta, cantidad) VALUES ('"+idPrestamo+"', '"+idHerramienta+"', '"+cantidad+"')");
		this.actualizarDetalles(idPrestamo);
		this.actualizarPrestamos();
	}
	/**
		* Metodo para eliminar un prestamo
		* @param prestamo Recibe un objeto tipo Prestamo
	*/
	public void eliminarPrestamo(Prestamo prestamo){
		conect.ejecutarSentencia("DELETE FROM Prestamo WHERE idPrestamo='"+prestamo.getIdPrestamo()+"'");
		this.actualizarPrestamos();
	}
	public void eliminarDetalle(DetallePrestamo detalle){
		conect.ejecutarSentencia("DELETE FROM DetallePrestamo WHERE idPrestamo='"+detalle.getIdPrestamo()+"' AND idHerramienta='"+detalle.getIdHerramienta()+"'");
		this.actualizarDetalles(detalle.getIdPrestamo());
	}
	/**
		* Metodo para obtener la fecha del sistema
		* @return Retora un String con la fecha del sistema
	*/
	public String generarFecha() {
		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
		return formateador.format(ahora);
	}
	public String formatearFecha(Date fecha){
		SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
		return formateador.format(fecha);
	}
	/**
		* Actualiza un prestamo especifico
		* @param prestamo Recibe el prestamo con los nuevos valores
	*/
	public void actualizarPrestamo(Prestamo prestamo){
		conect.ejecutarSentencia("UPDATE Prestamo SET idCliente='"+prestamo.getIdCliente()+"',  idEmpleado='"+prestamo.getIdEmpleado()+"', fecha='"+prestamo.getFecha()+"', fechaEntrega='"+prestamo.getFechaEntrega()+"',cantidad='"+prestamo.getCantidad()+"', estado='"+prestamo.getEstado()+"'");
		this.actualizarPrestamos();
	}
	/**
		* Metodo para actualizar los datos de un detalle
		* @param detalle DetallePrestamo con el valor que se actualizara
	*/
	public void actualizarDetalle(DetallePrestamo detalle){
		conect.ejecutarSentencia("UPDATE DetallePrestamo SET idPrestamo='"+detalle.getIdPrestamo()+"', idHerramienta='"+detalle.getIdHerramienta()+"', cantidad='"+detalle.getCantidad()+"'");
		this.actualizarDetalles(detalle.getIdPrestamo());
	}
	/**
		* Actualiza todos los prestamos en la lista
	*/
	public void actualizarPrestamos(){
		ResultSet result = conect.ejecutarConsulta("SELECT Prestamo.idPrestamo, Cliente.idCliente, Cliente.nombre AS ClienteNombre, Empleado.idEmpleado, Empleado.nombre AS Empleado, Prestamo.fecha, Prestamo.fechaEntrega, Prestamo.cantidad, Prestamo.estado FROM Prestamo INNER JOIN Cliente ON Prestamo.idCliente=Cliente.idCliente INNER JOIN Empleado ON Prestamo.idEmpleado=Empleado.idEmpleado");
		if(result!=null){
			prestamos.clear();
			try{
				while(result.next()){
					Prestamo prestamo;
					if(!result.getString("estado").equals("Entregado")){
						if(verificarFecha(result.getString("fechaEntrega"))){
							prestamo = new Prestamo(result.getInt("idPrestamo"), result.getInt("idCliente"), result.getString("ClienteNombre"), result.getInt("idEmpleado"), result.getString("Empleado"), result.getString("fecha"), result.getString("fechaEntrega"), result.getInt("cantidad"), result.getString("estado"));	
						}else{
							prestamo = new Prestamo(result.getInt("idPrestamo"), result.getInt("idCliente"), result.getString("ClienteNombre"), result.getInt("idEmpleado"), result.getString("Empleado"), result.getString("fecha"), result.getString("fechaEntrega"), result.getInt("cantidad"), "Fuera de fecha");
						}					
					}else{
						prestamo = new Prestamo(result.getInt("idPrestamo"), result.getInt("idCliente"), result.getString("ClienteNombre"), result.getInt("idEmpleado"), result.getString("Empleado"), result.getString("fecha"), result.getString("fechaEntrega"), result.getInt("cantidad"), result.getString("estado"));	
					}
					prestamos.add(prestamo);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
	/**
		* Metodo para cambiar el estado de un prestamo a entregado
		* @param prestamo Prestamo al que se le cambiara el estado
	*/
	public void cambiarEstado(Prestamo prestamo){
		conect.ejecutarSentencia("UPDATE Prestamo SET estado='Entregado' WHERE idPrestamo='"+prestamo.getIdPrestamo()+"'");
		this.actualizarPrestamos();
	}
	/**
		* Metodo para verificar si una fecha es anterior a la fecha del sistema
		* @param fecha String con la fecha que se verificara
		* @return Retorna un boolean respectivo a si la fecha es anterior o no
	*/
	public boolean verificarFecha(String fecha){
		String[] partes=fecha.split("/");
		String[] now=(this.generarFecha()).split("/");
		if(partes.length==3){
			try{
				if(Integer.parseInt(partes[2])>=Integer.parseInt(now[2])){
					if(Integer.parseInt(partes[2])>Integer.parseInt(now[2])){
						return true;
					}else if(Integer.parseInt(partes[2])==Integer.parseInt(now[2])){
						if(Integer.parseInt(partes[1])>=Integer.parseInt(now[1])){
							if(Integer.parseInt(partes[1])>Integer.parseInt(now[1])){
								return true;
							}else if(Integer.parseInt(partes[1])==Integer.parseInt(now[1])){
								if(Integer.parseInt(partes[0])>=Integer.parseInt(now[0])){
									return true;
								} else{
									return false;
								}
							}
						}else
							return false;
					}
				}else
					return false;
			}catch(NumberFormatException e){
				return false;
			}
		}
		return false;
	}
	/**
		* Metodo para actualizar los detalles de un Prestamo
		* @param idPrestamo Int con el id de un prestamo para actualizar sus detalles
	*/
	public void actualizarDetalles(int idPrestamo){
		ResultSet result = conect.ejecutarConsulta("SELECT DetallePrestamo.idPrestamo, DetallePrestamo.idHerramienta, DetallePrestamo.cantidad, Herramienta.nombre AS NombreHerramienta FROM DetallePrestamo INNER JOIN Herramienta ON Herramienta.idHerramienta=DetallePrestamo.idHerramienta WHERE DetallePrestamo.idPrestamo='"+idPrestamo+"'");
		if(result!=null){
			detalles.clear();
			try{
				while(result.next()){
					DetallePrestamo detalle = new DetallePrestamo(result.getInt("idPrestamo"), result.getInt("idHerramienta"), result.getInt("cantidad"), result.getString("NombreHerramienta"));
					detalles.add(detalle);
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}
	}
	/**
		* Obtiene la lista de prestamos
		* @return Retorna una lista ObservableList con los prestamos
	*/
	public ObservableList<Prestamo> obtenerPrestamos(){
		this.actualizarPrestamos();
		return this.prestamos;
	}
	/**
		* Metodo para obtener los detalles de un prestamo
		* @param idPrestamo Int con el id del prestamo del que se obtendran detalles
		* @return Retorna un ObservableList con los detalles del prestamo
	*/
	public ObservableList<DetallePrestamo> obtenerDetalles(int idPrestamo){
		this.actualizarDetalles(idPrestamo);
		return this.detalles;
	}
	/**
		* Metodo que actualizar los prestamos exceptuando los que ha sido entregados
	*/
	public void actualizarOutEntregate(){
		this.outEntregate.clear();
		for(Prestamo presta:prestamos){
			if(!presta.getEstado().equals("Entregado"))
				outEntregate.add(presta);
		}
	}
	/**
		* Metodo para obtener los prestamos exceptuando a los que ya fueron entregados
		* @return Retorna un ObservableList con los prestamos sin los que han sido entregados
	*/
	public ObservableList<Prestamo> obtenerOutEntregate(){
		this.actualizarOutEntregate();
		return this.outEntregate;
	}
	/**
		* Metodo que actualiza los prestamos que ya fueron entregados
	*/
	public void actualizarEntregados(){
		this.entregados.clear();
		for(Prestamo presta:prestamos){
			if(presta.getEstado().equals("Entregado"))
				entregados.add(presta);
		}
	}
	/**
		* Metodo para obtener los prestamos que ya fueron entregados
		* @return Retorna un ObservableList con los prestamos entregados
	*/
	public ObservableList<Prestamo> obtenerEntregados(){
		this.actualizarEntregados();
		return this.entregados;
	}
	/**
		* Metodo para actualizar los prestamos que estan fuera de fecha
	*/
	public void actualizarFueraFecha(){
		this.fueraFecha.clear();
		for(Prestamo presta:prestamos){
			if(presta.getEstado().equals("Fuera de fecha"))
				fueraFecha.add(presta);
		}
	}
	/**
		* Metodo para obtener los prestamos fuera de fecha
		* @return Retorna un ObservableList con los prestamos fuera de fecha
	*/
	public ObservableList<Prestamo> obtenerFueraFecha(){
		this.actualizarFueraFecha();
		return this.fueraFecha;
	}
	/**
		* Metodo para actualizar los prestamos que estan pendientes
	*/
	public void actualizarPendientes(){
		this.pendientes.clear();
		for(Prestamo presta:prestamos){
			if(presta.getEstado().equals("Pendiente"))
				pendientes.add(presta);
		}
	}
	/**
		* Metodo para obtener los prestamos en estado pendiente
		* @return Retorna un ObservableList con los prestamos pendientes
	*/
	public ObservableList<Prestamo> obtenerPendientes(){
		this.actualizarPendientes();
		return this.pendientes;
	}
}
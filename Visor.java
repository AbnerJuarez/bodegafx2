package org.abnerjuarez.visual;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Accordion;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Tooltip;
import javafx.event.ActionEvent;

import java.util.ArrayList;
import javafx.util.Callback;

import org.abnerjuarez.beans.Prestamo;
import org.abnerjuarez.manejadores.ManejadorPrestamo;
import org.abnerjuarez.recursos.Eliminador;

/**
	* Clase encargada de administrar el modulo visor del programa
	* @author Abner  A. Juarez D.
*/
public class Visor extends Application implements EventHandler<Event>{
	//Propiedades
	private TitledPane tpGeneral, tpPendientes, tpEntregados, tpFueraFecha;
	private ToolBar toolbar;
	private Button btEntregar, btActualizar;
	private BorderPane bpPrincipal;
	private Stage stgPrincipal;
	private Scene scnPrincipal;
	private ManejadorPrestamo mngPrestamo;
	private Accordion acPrincipal;
	private Eliminador eliminador;
	private TableView<Prestamo> tvPrestamos, tvPendientes, tvEntregados, tvFueraFecha;

	/**
		* Metodo encargado de iniciar la clase heredade de Application
		* @param primary Stage con la ventana primaria de la clase
	*/
	public void start(Stage primary){
	}
	/**
		* Constructor de la clase con  un solo parametro
		* @param mp ManejadorPrestamo a inyectar en la clase
	*/
	public Visor(ManejadorPrestamo mp){
		this.mngPrestamo=mp;
	}
	/**
		* Metodo local encargado de iniciar la clase
	*/
	public void iniciar(){
		this.eliminador = new Eliminador(mngPrestamo);
		this.bpPrincipal = new BorderPane();
		this.btEntregar = new Button("Entregado");
		this.btEntregar.addEventHandler(ActionEvent.ACTION, this);
		this.btEntregar.setTooltip(new Tooltip("Cambiar estado a \"Entregado\""));
		this.btActualizar = new Button("Actualizar");
		this.btActualizar.addEventHandler(ActionEvent.ACTION, this);
		this.btActualizar.setTooltip(new Tooltip("Actualizar los prestamos"));
		this.toolbar = new ToolBar(btEntregar, btActualizar);
		this.tpGeneral = new TitledPane("Vista General", this.getPrestamos());
		this.tpEntregados = new TitledPane("Entregados", this.getEntregados());
		this.tpPendientes = new TitledPane("Pendientes", this.getPendientes());
		this.tpFueraFecha = new TitledPane("Fuera de fecha", this.getFueraFecha());
		this.acPrincipal = new Accordion();
		this.acPrincipal.getPanes().addAll(tpGeneral, tpEntregados, tpPendientes, tpFueraFecha);
		this.acPrincipal.setExpandedPane(tpGeneral);
		this.bpPrincipal.setTop(toolbar);
		this.bpPrincipal.setCenter(acPrincipal);
		this.scnPrincipal = new Scene(bpPrincipal);
		this.scnPrincipal.getStylesheets().add("visor.css");
		this.stgPrincipal = new Stage();
		this.stgPrincipal.setTitle("Modulo Visor");
		this.stgPrincipal.setScene(scnPrincipal);
		this.stgPrincipal.show();
	}
	/**
		* Metodo local encargado de cerrar la clase
	*/	
	public void cerrar(){
		this.stgPrincipal.close();
	}
	/**
		* Metodo encargado de manejar los eventos de  la clase
		* @param event Event generado por la clase
	*/
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource().equals(btEntregar)){
				if(acPrincipal.getExpandedPane().equals(tpPendientes)){
					this.entregar(tvPendientes.getSelectionModel().getSelectedItem());
				}else if(acPrincipal.getExpandedPane().equals(tpGeneral)){
					this.entregar(tvPrestamos.getSelectionModel().getSelectedItem());
				}else if(acPrincipal.getExpandedPane().equals(tpFueraFecha)){
					this.entregar(tvFueraFecha.getSelectionModel().getSelectedItem());
				}
			}else if(event.getSource().equals(btActualizar)){
				tpFueraFecha.setContent(this.getFueraFecha());
				tpPendientes.setContent(this.getPendientes());
				tpGeneral.setContent(this.getPrestamos());
				tpEntregados.setContent(this.getEntregados());
			}
		}
	}
	/**
		* Metodo para cambiar el estado de un prestamo
		* @param prestamo Prestamo al que se le cambiara el estado
	*/
	public void entregar(Prestamo prestamo){
		try{
			mngPrestamo.cambiarEstado(prestamo);
			eliminador.eliminarDetalles(prestamo.getIdPrestamo());
			mngPrestamo.actualizarPrestamos();
			mngPrestamo.actualizarOutEntregate();
			mngPrestamo.actualizarPendientes();
			mngPrestamo.actualizarFueraFecha();
			mngPrestamo.actualizarEntregados();
		}catch(NullPointerException e){
			e.printStackTrace();
		}
	}
	/**
		* Metodo para obtener todos los prestamos del programa
		* @return Retorna un TableView con todos los prestamos
	*/
	public TableView getPrestamos(){
		tvPrestamos = new TableView<Prestamo>(mngPrestamo.obtenerPrestamos());

		TableColumn<Prestamo, Integer> columnaIdPrestamo=new TableColumn<Prestamo, Integer>("Id Prestamo");
		columnaIdPrestamo.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("idPrestamo"));
		columnaIdPrestamo.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreCliente=new TableColumn<Prestamo, String>("Cliente");
		columnaNombreCliente.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreCliente"));
		columnaNombreCliente.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreEmpleado=new TableColumn<Prestamo, String>("Empleado");
		columnaNombreEmpleado.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreEmpleado"));
		columnaNombreEmpleado.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFecha=new TableColumn<Prestamo, String>("Fecha");
		columnaFecha.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fecha"));
		columnaFecha.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFechaEntrega=new TableColumn<Prestamo, String>("Fecha de Entrega");
		columnaFechaEntrega.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fechaEntrega"));
		columnaFechaEntrega.setMinWidth(120);
		TableColumn<Prestamo, Integer> columnaCantidad=new TableColumn<Prestamo, Integer>("Cantidad");
		columnaCantidad.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("cantidad"));
		columnaCantidad.setMinWidth(120);
		TableColumn columnaEstado = new TableColumn("Estado");  
		columnaEstado.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("estado"));
		columnaEstado.setMinWidth(120);
		columnaEstado.setCellFactory(new Callback<TableColumn, TableCell>(){
			public TableCell call(TableColumn param){
				return new TableCell<Prestamo, String>(){
				@Override
				public void updateItem(String item, boolean empty){
						super.updateItem(item, empty); 
						if(!isEmpty()) {
							if(item.contains("Entregado")){
								//Azul
								this.getTableRow().setStyle("-fx-background-color:#2D928F");
							}else if(item.contains("Pendiente")){
								//Amarillo
								this.getTableRow().setStyle("-fx-background-color:#566100");
							}else if(item.contains("Fuera de fecha")){
								//Rojo
								this.getTableRow().setStyle("-fx-background-color:#922D34");
							}
							setText(item);
						}
					}
		    	        	};
	        		}
	    	});

		tvPrestamos.getColumns().setAll(columnaIdPrestamo, columnaNombreCliente, columnaNombreEmpleado, columnaFecha, columnaFechaEntrega, columnaCantidad, columnaEstado);

		return tvPrestamos;
	}
	/**
		* Metodo encargado de obtener los prestamos pendientes del programa
		* @return Retorna un TableView con los prestamos pendientes
	*/
	public TableView<Prestamo> getPendientes(){
		tvPendientes = new TableView<Prestamo>(mngPrestamo.obtenerPendientes());

		TableColumn<Prestamo, Integer> columnaIdPrestamo=new TableColumn<Prestamo, Integer>("Id Prestamo");
		columnaIdPrestamo.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("idPrestamo"));
		columnaIdPrestamo.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreCliente=new TableColumn<Prestamo, String>("Cliente");
		columnaNombreCliente.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreCliente"));
		columnaNombreCliente.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreEmpleado=new TableColumn<Prestamo, String>("Empleado");
		columnaNombreEmpleado.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreEmpleado"));
		columnaNombreEmpleado.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFecha=new TableColumn<Prestamo, String>("Fecha");
		columnaFecha.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fecha"));
		columnaFecha.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFechaEntrega=new TableColumn<Prestamo, String>("Fecha de Entrega");
		columnaFechaEntrega.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fechaEntrega"));
		columnaFechaEntrega.setMinWidth(120);
		TableColumn<Prestamo, Integer> columnaCantidad=new TableColumn<Prestamo, Integer>("Cantidad");
		columnaCantidad.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("cantidad"));
		columnaCantidad.setMinWidth(120);
		TableColumn columnaEstado = new TableColumn("Estado");  
		columnaEstado.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("estado"));
		columnaEstado.setMinWidth(120);
		columnaEstado.setCellFactory(new Callback<TableColumn, TableCell>(){
			public TableCell call(TableColumn param){
				return new TableCell<Prestamo, String>(){
				@Override
				public void updateItem(String item, boolean empty) { 
						super.updateItem(item, empty); 
						if(!isEmpty()){
							if(item.contains("Pendiente")){
								//Amarillo
								this.getTableRow().setStyle("-fx-background-color:#566100");
							}
							setText(item);
						}
					}
				};
			}
		});

		tvPendientes.getColumns().setAll(columnaIdPrestamo, columnaNombreCliente, columnaNombreEmpleado, columnaFecha, columnaFechaEntrega, columnaCantidad, columnaEstado);

		return tvPendientes;
	}
	/**
		* Metodo para obtener los prestamos entregados del programa
		* @return Retorna una TableView con todos los prestamos engregados
	*/
	public TableView<Prestamo> getEntregados(){
		tvEntregados = new TableView<Prestamo>(mngPrestamo.obtenerEntregados());

		TableColumn<Prestamo, Integer> columnaIdPrestamo=new TableColumn<Prestamo, Integer>("Id Prestamo");
		columnaIdPrestamo.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("idPrestamo"));
		columnaIdPrestamo.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreCliente=new TableColumn<Prestamo, String>("Cliente");
		columnaNombreCliente.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreCliente"));
		columnaNombreCliente.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreEmpleado=new TableColumn<Prestamo, String>("Empleado");
		columnaNombreEmpleado.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreEmpleado"));
		columnaNombreEmpleado.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFecha=new TableColumn<Prestamo, String>("Fecha");
		columnaFecha.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fecha"));
		columnaFecha.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFechaEntrega=new TableColumn<Prestamo, String>("Fecha de Entrega");
		columnaFechaEntrega.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fechaEntrega"));
		columnaFechaEntrega.setMinWidth(120);
		TableColumn<Prestamo, Integer> columnaCantidad=new TableColumn<Prestamo, Integer>("Cantidad");
		columnaCantidad.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("cantidad"));
		columnaCantidad.setMinWidth(120);
		TableColumn columnaEstado = new TableColumn("Estado");  
		columnaEstado.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("estado"));
		columnaEstado.setMinWidth(120);
		columnaEstado.setCellFactory(new Callback<TableColumn, TableCell>(){
			public TableCell call(TableColumn param){
				return new TableCell<Prestamo, String>(){
				@Override
				public void updateItem(String item, boolean empty) { 
						super.updateItem(item, empty); 
						if(!isEmpty()){
							if(item.contains("Entregado")){
								//Azul
								this.getTableRow().setStyle("-fx-background-color:#2D928F");
							}							
							setText(item);
						}
					}
				};
			}
		});

		tvEntregados.getColumns().setAll(columnaIdPrestamo, columnaNombreCliente, columnaNombreEmpleado, columnaFecha, columnaFechaEntrega, columnaCantidad, columnaEstado);

		return tvEntregados;
	}
	/**
		* Metodo para obtener los prestamos que esta fuera de fecha
		* @return Retorna un TableView con los prestamos fuera de fecha
	*/
	public TableView<Prestamo> getFueraFecha(){
		tvFueraFecha = new TableView<Prestamo>(mngPrestamo.obtenerFueraFecha());

		TableColumn<Prestamo, Integer> columnaIdPrestamo=new TableColumn<Prestamo, Integer>("Id Prestamo");
		columnaIdPrestamo.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("idPrestamo"));
		columnaIdPrestamo.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreCliente=new TableColumn<Prestamo, String>("Cliente");
		columnaNombreCliente.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreCliente"));
		columnaNombreCliente.setMinWidth(120);
		TableColumn<Prestamo, String> columnaNombreEmpleado=new TableColumn<Prestamo, String>("Empleado");
		columnaNombreEmpleado.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("nombreEmpleado"));
		columnaNombreEmpleado.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFecha=new TableColumn<Prestamo, String>("Fecha");
		columnaFecha.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fecha"));
		columnaFecha.setMinWidth(120);
		TableColumn<Prestamo, String> columnaFechaEntrega=new TableColumn<Prestamo, String>("Fecha de Entrega");
		columnaFechaEntrega.setCellValueFactory(new PropertyValueFactory<Prestamo, String>("fechaEntrega"));
		columnaFechaEntrega.setMinWidth(120);
		TableColumn<Prestamo, Integer> columnaCantidad=new TableColumn<Prestamo, Integer>("Cantidad");
		columnaCantidad.setCellValueFactory(new PropertyValueFactory<Prestamo, Integer>("cantidad"));
		columnaCantidad.setMinWidth(120);
		TableColumn columnaEstado = new TableColumn("Estado");  
		columnaEstado.setCellValueFactory(new PropertyValueFactory<Prestamo,String>("estado"));
		columnaEstado.setMinWidth(120);
		columnaEstado.setCellFactory(new Callback<TableColumn, TableCell>(){
			public TableCell call(TableColumn param){
				return new TableCell<Prestamo, String>(){
				@Override
				public void updateItem(String item, boolean empty) { 
						super.updateItem(item, empty); 
						if(!isEmpty()){
							if(item.contains("Fuera de fecha")){
								//Rojo
								this.getTableRow().setStyle("-fx-background-color:#922D34");
							}							
							setText(item);
						}
					}
				};
			}
		});

		tvFueraFecha.getColumns().setAll(columnaIdPrestamo, columnaNombreCliente, columnaNombreEmpleado, columnaFecha, columnaFechaEntrega, columnaCantidad, columnaEstado);

		return tvFueraFecha;
	}
}
package org.abnerjuarez.manejadores;

import org.abnerjuarez.db.Conection;
import org.abnerjuarez.beans.Cliente;

import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import java.util.ArrayList;
import java.util.AbstractList;
import javafx.beans.property.SimpleListProperty;

/**
	* Clase de tipo manejador que administra el beans Cliente
	* @author Abner A. Juarez D.
*/
public class ManejadorCliente{
	//Propiedades
	private ObservableList<Cliente> clientes, encontrados;
	private Conection conection;
	/**
		* Constructor de la clase ManejadorCliente
		* @param conection Recibe un objeto tipo Conection para conectarse a la base de datos
	*/
	public ManejadorCliente(Conection conection){
		this.conection=conection;
		this.clientes=FXCollections.observableArrayList();
		this.encontrados=FXCollections.observableArrayList();
	}
	/**
		* Metodo para agregar clientes a la base de datos
		* @param nombre String con el valor del nombre del cliente
		* @param apellido String con el valor del apellido del cliente
		* @param carne String con el valor del carne del cliente
		* @param seccion String con el valor de la seccion del cliente
		* @param jornada String con el valor de la jornada del cliente
	*/
	public void agregarCliente(String nombre, String apellido, String carne, String seccion, String jornada){
		if(this.buscarCarne(carne)==false){
			conection.ejecutarSentencia("INSERT INTO Cliente(nombre, apellido, carne, seccion, jornada) VALUES('"+nombre+"', '"+apellido+"', '"+carne+"', '"+seccion+"', '"+jornada+"')");
			this.actualizarClientes();
		}
	}
	/**
		* Metodo para actualizar la lista de clientes
	*/
	public void actualizarClientes(){
		ResultSet result = conection.ejecutarConsulta("SELECT idCliente, nombre, apellido, carne, seccion, jornada FROM Cliente");
		if(result != null){
			clientes.clear();
			try{
				while(result.next()){
					Cliente cliente= new Cliente(result.getInt("idCliente"), result.getString("nombre"), result.getString("apellido"), result.getString("carne"), result.getString("seccion"), result.getString("jornada"));
					clientes.add(cliente);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
	/**
		* Metodo para obtener la lista de los clientes en la base de datos
		* @return Retorna un ObservableList con la lista de los clientes
	*/
	public ObservableList<Cliente> obtenerClientes(){
		this.actualizarClientes();
		return this.clientes;
	}
	/**
		* Metodo para eliminar un cliente de la base de datos
		* @param cliente Recibe un objeto tipo Cliente para eliminarlo de la base de datos
	*/
	public void eliminarCliente(Cliente cliente){
		conection.ejecutarSentencia("DELETE FROM Cliente WHERE idCliente="+cliente.getIdUsuario());
		actualizarClientes();
	}
	/**
		* Metodo para actualizar un cliente de la base de dato
		* @param cliente Recibe un objeto tipo cliente para modificarlo en la base de datos
	*/
	public void modificarCliente(Cliente cliente){
		conection.ejecutarSentencia("UPDATE Cliente SET nombre='"+cliente.getNombre()+"', apellido='"+cliente.getApellido()+"', carne='"+cliente.getCarne()+"', seccion='"+cliente.getSeccion()+"', jornada='"+cliente.getJornada()+"' WHERE idCliente="+cliente.getIdUsuario());
		actualizarClientes();
	}
	/**
		* Metodo para verificar el numero de carne del cliente
		* @param carne Recibe un String con el valor del carne a buscar
		* @return Retorna un boolean respectivo a si lo encontro o no
	*/
	public boolean buscarCarne(String carne){
		ResultSet result = conection.ejecutarConsulta("SELECT idCliente, nombre, apellido, carne, seccion, jornada FROM Cliente WHERE carne='"+carne+"'");
		try{
			while(result.next()){
				return true;
			}
		}catch(SQLException sql){}
		return false;
	}
	/**
		* Metodo para buscar un cliente en la base datos segun su carne
		* @param carne Recibe un String con el valor del carne a buscar
		* @return Retorna un objeto tipo cliente con el valor del cliente encontrado
	*/
	public Cliente buscarCliente(String carne){
		ResultSet result = conection.ejecutarConsulta("SELECT idCliente, nombre, apellido, carne, seccion, jornada FROM Cliente WHERE carne='"+carne+"'");
		if(result!=null){
			try{
				while(result.next()){
					Cliente cliente= new Cliente(result.getInt("idCliente"), result.getString("nombre"), result.getString("apellido"), result.getString("carne"), result.getString("seccion"), result.getString("jornada"));
					return cliente;
				}
			}catch(SQLException sql){
				sql.printStackTrace();
			}
		}
		return null;
	}
	/**
		* Metodo para buscar un cliente en la lista de clientes
		* @param idCliente Int con el valor del Id del cliente a buscar
		* @return Retorna un objeto Cliente con el cliente encontrado
	*/
	public Cliente buscarCliente(int idCliente){
		this.actualizarClientes();
		for(Cliente cliente:clientes){
			if(cliente.getIdUsuario()==idCliente)
				return cliente;
		}
		return null;
	}
	/**
		* Metodo para actualizar los clientes encontrados en la busqueda
		* @param parametro String con el parametro de busqueda
	*/
	public void actualizarEncontrados(String parametro){
		this.actualizarClientes();
		for(Cliente cliente:clientes){
			parametro = parametro.toLowerCase();
			if(cliente.getNombre().toLowerCase().contains(parametro) || cliente.getApellido().toLowerCase().contains(parametro) || cliente.getCarne().toLowerCase().contains(parametro) || cliente.getSeccion().toLowerCase().contains(parametro) || cliente.getJornada().toLowerCase().contains(parametro)){
				encontrados.add(cliente);
			}
		}
	}
	/**
		* Metodo para buscar clientes segun un parametro
		* @param parametro String con el parametro de la busqueda
		* @return Retorna un ObservableList con los clientes encontrados;
	*/
	public ObservableList<Cliente> buscarClienteAvanzada(String parametro){
		this.actualizarEncontrados(parametro);
		return encontrados;
	}
}
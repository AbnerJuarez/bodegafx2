package org.abnerjuarez.beans;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
	* Clase tipo beans abstracta que contiene las funciones basicas de un usuario
	* @author Abner A. Juarez D.
*/
public abstract class Usuario{
	//Propiedades de la clase
	StringProperty nombre, apellido, modulos, pass, id;
	IntegerProperty idUsuario;
	/**
		* Metodo que instancia las propiedades
	*/
	public void instanciar(){
		nombre = new SimpleStringProperty();
		id = new SimpleStringProperty();
		apellido = new SimpleStringProperty();
		pass = new SimpleStringProperty();
		modulos = new SimpleStringProperty();
		idUsuario = new SimpleIntegerProperty();
	}
	//Setter
	/**
		* Metodo para settear idUsuario generado por la base de datos
		* @param idUsuario Int con el valor IdUsuario
	*/
	public void setIdUsuario(int idUsuario){
		this.idUsuario.set(idUsuario);
	}
	/**
		* Metodo para settear el id elegido por el usuario
		* @param id String con el valor del id
	*/
	public void setId(String id){
		this.id.set(id);
	}
	/**
		* Metodo para settear el nombre del usuario
		* @param nombre String con el valor del nombre
	*/
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	/**
		* Metodo para setter el apellido del usuario
		* @param apellido String con el valor del apellido
	*/
	public void setApellido(String apellido){
		this.apellido.set(apellido);
	}
	/**
		* Metodo para settear los modulos del usuario
		* @param modulos String con el valor de los modulos
	*/
	public void setModulos(String modulos){
		this.modulos.set(modulos);
	}
	/**
		* Metodo para setter el password del usuario
		* @param pass String con el valor del password
	*/
	public void setPassword(String pass){
		this.pass.set(pass);
	}
	//Getter
	/**
		* Metodo para obtener el idUsuario del usuario
		* @return Retorna un int con el valor del idUsuario
	*/
	public int getIdUsuario(){
		return this.idUsuario.get();
	}
	/**
		* Metodo para obtener el id del usuario
		* @return Retorna un String con el valor del id
	*/
	public String getId(){
		return this.id.get();
	}
	/**
		* Metodo para obtener el nombre del usuario
		* @return Retorna un String con el valor del nombre
	*/
	public String getNombre(){
		return this.nombre.get();
	}
	/**
		* Metodo para obtener el apellido del usuario
		* @return Retorna un String con el apellido del usuario
	*/
	public String getApellido(){
		return this.apellido.get();
	}
	/**
		* Metodo para obtener los modulos del usuario
		* @return Retorna un String con el valor de los modulos
	*/
	public String getModulos(){
		return this.modulos.get();
	}
	/**
		* Obtiene el password del usuario
		* @return Retorna un String con el valor del password 
	*/
	public String getPassword(){
		return this.pass.get();
	}
}